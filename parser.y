/*

Copyright 2006-2021 by

Laboratoire de l'Informatique du Parallelisme,
UMR CNRS - ENS Lyon - UCB Lyon 1 - INRIA 5668,

Laboratoire d'Informatique de Paris 6, equipe PEQUAN,
UPMC Universite Paris 06 - CNRS - UMR 7606 - LIP6, Paris, France,

Laboratoire d'Informatique de Paris 6 - Équipe PEQUAN
Sorbonne Universités
UPMC Univ Paris 06
UMR 7606, LIP6
Boîte Courrier 169
4, place Jussieu
F-75252 Paris Cedex 05
France

and by

Department of Computer Science & Engineering
UAA College of Engineering
University of Alaska Anchorage.

Contributors Ch. Lauter, S. Chevillard

christoph.lauter@christoph-lauter.org
sylvain.chevillard@ens-lyon.org

This software is a computer program whose purpose is to provide an
environment for safe floating-point code development. It is
particularly targeted to the automated implementation of
mathematical floating-point libraries (libm). Amongst other features,
it offers a certified infinity norm, an automatic polynomial
implementer and a fast Remez algorithm.

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.

This program is distributed WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

*/

%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "base-functions.h"
#include "expression.h"
#include "assignment.h"
#include "chain.h"
#include "general.h"
#include "execute.h"

#include "parser.h"
#include "library.h"
#include "sollya-help.h"
#include "version.h"

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

/* Mess with the mallocs used by the parser */
extern void *parserCalloc(size_t, size_t);
extern void *parserMalloc(size_t);
extern void *parserRealloc(void *, size_t);
extern void parserFree(void *);
#undef malloc
#undef realloc
#undef calloc
#undef free
#define malloc parserMalloc
#define realloc parserRealloc
#define calloc parserCalloc
#define free parserFree
/* End of the malloc mess */

#define YYERROR_VERBOSE 1
#define YYFPRINTF sollyaFprintf

extern int yylex(YYSTYPE *lvalp, void *scanner);
extern FILE *yyget_in(void *scanner);
extern char *getCurrentLexSymbol();

void yyerror(void *scanner, const char *message) {
  char *str;
  if (!feof(yyget_in(scanner))) {
    str = getCurrentLexSymbol();
    printMessage(1,SOLLYA_MSG_SYNTAX_ERROR_ENCOUNTERED_WHILE_PARSING,"Warning: %s.\nThe last symbol read has been \"%s\".\nWill skip input until next semicolon after the unexpected token. May leak memory.\n",message,str);
    safeFree(str);
    promptToBePrinted = 1;
    lastWasSyntaxError = 1;
    considerDyingOnError();
  } 
}

int parserCheckEof() {
  FILE *myFd;

  myFd = yyget_in(scanner);
  if (myFd == NULL) return 0;
  
  return feof(myFd);
}

/* #define WARN_IF_NO_HELP_TEXT 1 */
 
%}

%defines

%expect 2

%lex-param { void* scanner }
%parse-param { void* scanner }

%define api.pure

%union {
  doubleNode *dblnode;
  struct entryStruct *association;
  char *value;
  node *tree;
  chain *list;
  int *integerval;
  int count;
  void *other;
};



%token  <value> CONSTANTTOKEN "decimal constant"
%token  <value> MIDPOINTCONSTANTTOKEN "interval"
%token  <value> DYADICCONSTANTTOKEN "dyadic constant"
%token  <value> HEXCONSTANTTOKEN "constant in memory notation"
%token  <value> HEXADECIMALCONSTANTTOKEN "hexadecimal constant"
%token  <value> BINARYCONSTANTTOKEN "binary constant"

%token  PITOKEN "pi"

%token  <value> IDENTIFIERTOKEN "identifier"

%token  <value> STRINGTOKEN "character string"

%token  LPARTOKEN "("
%token  RPARTOKEN ")"
%token  LBRACKETTOKEN "["
%token  RBRACKETTOKEN "]"
%token  EQUALTOKEN "="
%token  ASSIGNEQUALTOKEN ":="
%token  COMPAREEQUALTOKEN "=="
%token  COMMATOKEN ","
%token  EXCLAMATIONTOKEN "!"
%token  SEMICOLONTOKEN ";"
%token  STARLEFTANGLETOKEN "*<"
%token  LEFTANGLETOKEN "<"
%token  RIGHTANGLEUNDERSCORETOKEN ">_"
%token  RIGHTANGLEDOTTOKEN ">."
%token  RIGHTANGLESTARTOKEN ">*"
%token  RIGHTANGLETOKEN ">"
%token  DOTSTOKEN "..."
%token  DOTTOKEN "."
%token  QUESTIONMARKTOKEN "?"
%token  VERTBARTOKEN "|"
%token  ATTOKEN "@"
%token  DOUBLECOLONTOKEN "::"
%token  COLONTOKEN ":"
%token  DOTCOLONTOKEN ".:"
%token  COLONDOTTOKEN ":."
%token  EXCLAMATIONEQUALTOKEN "!="
%token  APPROXTOKEN "~"
%token  ANDTOKEN "&&"
%token  ORTOKEN "||"

%token  PLUSTOKEN "+"
%token  MINUSTOKEN "-"
%token  MULTOKEN "*"
%token  DIVTOKEN "/"
%token  POWTOKEN "^"

%token  SQRTTOKEN "sqrt"
%token  EXPTOKEN "exp"
%token  FREEVARTOKEN "_x_"
%token  LOGTOKEN "log"
%token  LOG2TOKEN "log2"
%token  LOG10TOKEN "log10"
%token  SINTOKEN "sin"
%token  COSTOKEN "cos"
%token  TANTOKEN "tan"
%token  ASINTOKEN "asin"
%token  ACOSTOKEN "acos"
%token  ATANTOKEN "atan"
%token  SINHTOKEN "sinh"
%token  COSHTOKEN "cosh"
%token  TANHTOKEN "tanh"
%token  ASINHTOKEN "asinh"
%token  ACOSHTOKEN "acosh"
%token  ATANHTOKEN "atanh"
%token  ABSTOKEN "abs"
%token  ERFTOKEN "erf"
%token  ERFCTOKEN "erfc"
%token  LOG1PTOKEN "log1p"
%token  EXPM1TOKEN "expm1"
%token  DOUBLETOKEN "D"
%token  SINGLETOKEN "SG"
%token  HALFPRECISIONTOKEN "HP"
%token  QUADTOKEN "QD"
%token  DOUBLEDOUBLETOKEN "DD"
%token  TRIPLEDOUBLETOKEN "TD"
%token  DOUBLEEXTENDEDTOKEN "DE"
%token  CEILTOKEN "ceil"
%token  FLOORTOKEN "floor"
%token  NEARESTINTTOKEN "nearestint"

%token  HEADTOKEN "head"
%token  REVERTTOKEN "revert"
%token  SORTTOKEN "sort"
%token  TAILTOKEN "tail"
%token  MANTISSATOKEN "mantissa"
%token  EXPONENTTOKEN "exponent"
%token  PRECISIONTOKEN "precision"
%token  ROUNDCORRECTLYTOKEN "roundcorrectly"

%token  PRECTOKEN "prec"
%token  POINTSTOKEN "points"
%token  DIAMTOKEN "diam"
%token  DISPLAYTOKEN "display"
%token  VERBOSITYTOKEN "verbosity"
%token  SHOWMESSAGENUMBERSTOKEN "showmessagenumbers"
%token  CANONICALTOKEN "canonical"
%token  AUTOSIMPLIFYTOKEN "autosimplify"
%token  TAYLORRECURSIONSTOKEN "taylorrecursions"
%token  TIMINGTOKEN "timing"
%token  TIMETOKEN "time"
%token  FULLPARENTHESESTOKEN "fullparentheses"
%token  MIDPOINTMODETOKEN "midpointmode"
%token  DIEONERRORMODETOKEN "dieonerrormode"
%token  SUPPRESSWARNINGSTOKEN "roundingwarnings"
%token  RATIONALMODETOKEN "rationalmode"
%token  HOPITALRECURSIONSTOKEN "hopitalrecursions"

%token  ONTOKEN "on"
%token  OFFTOKEN "off"
%token  DYADICTOKEN "dyadic"
%token  POWERSTOKEN "powers"
%token  BINARYTOKEN "binary"
%token  HEXADECIMALTOKEN "hexadecimal"
%token  FILETOKEN "file"
%token  POSTSCRIPTTOKEN "postscript"
%token  POSTSCRIPTFILETOKEN "postscriptfile"
%token  PERTURBTOKEN "perturb"
%token  MINUSWORDTOKEN "RD"
%token  PLUSWORDTOKEN "RU"
%token  ZEROWORDTOKEN "RZ"
%token  NEARESTTOKEN "RN"
%token  HONORCOEFFPRECTOKEN "honorcoeffprec"
%token  TRUETOKEN "true"
%token  FALSETOKEN "false"
%token  DEFAULTTOKEN "default"
%token  MATCHTOKEN "match"
%token  WITHTOKEN "with"
%token  ABSOLUTETOKEN "absolute"
%token  DECIMALTOKEN "decimal"
%token  RELATIVETOKEN "relative"
%token  FIXEDTOKEN "fixed"
%token  FLOATINGTOKEN "floating"

%token  ERRORTOKEN "error"

%token  QUITTOKEN "quit"
%token  FALSEQUITTOKEN "quit in an included file"
%token  RESTARTTOKEN "restart"

%token  LIBRARYTOKEN "library"
%token  LIBRARYCONSTANTTOKEN "libraryconstant"

%token  DIFFTOKEN "diff"
%token  DIRTYSIMPLIFYTOKEN "dirtysimplify"
%token  REMEZTOKEN "remez"
%token  ANNOTATEFUNCTIONTOKEN "annotatefunction"
%token  BASHEVALUATETOKEN "bashevaluate"
%token  GETSUPPRESSEDMESSAGESTOKEN "getsuppressedmessages"
%token  GETBACKTRACETOKEN "getbacktrace"
%token  FPMINIMAXTOKEN "fpminimax"
%token  HORNERTOKEN "horner"
%token  EXPANDTOKEN "expand"
%token  SIMPLIFYSAFETOKEN "simplify"

%token  TAYLORTOKEN "taylor"
%token  TAYLORFORMTOKEN "taylorform"
%token  CHEBYSHEVFORMTOKEN "chebyshevform"
%token  AUTODIFFTOKEN "autodiff"
%token  DEGREETOKEN "degree"
%token  NUMERATORTOKEN "numerator"
%token  DENOMINATORTOKEN "denominator"
%token  SUBSTITUTETOKEN "substitute"
%token  COMPOSEPOLYNOMIALSTOKEN "composepolynomials"
%token  BEZOUTTOKEN "bezout"
%token  COEFFTOKEN "coeff"
%token  SUBPOLYTOKEN "subpoly"
%token  ROUNDCOEFFICIENTSTOKEN "roundcoefficients"
%token  RATIONALAPPROXTOKEN "rationalapprox"
%token  ACCURATEINFNORMTOKEN "accurateinfnorm"
%token  ROUNDTOFORMATTOKEN "round"
%token  EVALUATETOKEN "evaluate"
%token  LENGTHTOKEN "length"
%token  OBJECTNAMETOKEN "objectname"
%token  INFTOKEN "inf"
%token  MIDTOKEN "mid"
%token  SUPTOKEN "sup"
%token  MINTOKEN "min"
%token  MAXTOKEN "max"

%token  READXMLTOKEN "readxml"
%token  PARSETOKEN "parse"

%token  PRINTTOKEN "print"
%token  PRINTXMLTOKEN "printxml"
%token  PLOTTOKEN "plot"
%token  PRINTHEXATOKEN "printhexa"
%token  PRINTFLOATTOKEN "printfloat"
%token  PRINTBINARYTOKEN "printbinary"
%token  SUPPRESSMESSAGETOKEN "suppressmessage"
%token  UNSUPPRESSMESSAGETOKEN "unsuppressmessage"
%token  PRINTEXPANSIONTOKEN "printexpansion"
%token  BASHEXECUTETOKEN "bashexecute"
%token  EXTERNALPLOTTOKEN "externalplot"
%token  WRITETOKEN "write"
%token  ASCIIPLOTTOKEN "asciiplot"
%token  RENAMETOKEN "rename"
%token  BINDTOKEN "bind"

%token  INFNORMTOKEN "infnorm"
%token  SUPNORMTOKEN "supnorm"
%token  FINDZEROSTOKEN "findzeros"
%token  FPFINDZEROSTOKEN "fpfindzeros"
%token  DIRTYINFNORMTOKEN "dirtyinfnorm"
%token  GCDTOKEN "gcd"
%token  EUCLDIVTOKEN "div"
%token  EUCLMODTOKEN "mod"
%token  NUMBERROOTSTOKEN "numberroots"
%token  INTEGRALTOKEN "integral"
%token  DIRTYINTEGRALTOKEN "dirtyintegral"
%token  WORSTCASETOKEN "worstcase"
%token  IMPLEMENTPOLYTOKEN "implementpoly"
%token  IMPLEMENTCONSTTOKEN "implementconst"
%token  INTERPOLATETOKEN "interpolate"
%token  CHECKINFNORMTOKEN "checkinfnorm"
%token  ZERODENOMINATORSTOKEN "zerodenominators"
%token  ISEVALUABLETOKEN "isevaluable"
%token  SEARCHGALTOKEN "searchgal"
%token  GUESSDEGREETOKEN "guessdegree"
%token  DIRTYFINDZEROSTOKEN "dirtyfindzeros"

%token  IFTOKEN "if"
%token  THENTOKEN "then"
%token  ELSETOKEN "else"
%token  FORTOKEN "for"
%token  INTOKEN "in"
%token  FROMTOKEN "from"
%token  TOTOKEN "to"
%token  BYTOKEN "by"
%token  DOTOKEN "do"
%token  BEGINTOKEN "begin"
%token  ENDTOKEN "end"
%token  LEFTCURLYBRACETOKEN "{"
%token  RIGHTCURLYBRACETOKEN "}"
%token  WHILETOKEN "while"

%token  READFILETOKEN "readfile"

%token  ISBOUNDTOKEN "isbound"

%token  EXECUTETOKEN "execute"

%token  EXTERNALPROCTOKEN "externalproc"
%token  EXTERNALDATATOKEN "externaldata"
%token  VOIDTOKEN "void"
%token  CONSTANTTYPETOKEN "constant"
%token  FUNCTIONTOKEN "function"
%token  OBJECTTOKEN "object"
%token  RANGETOKEN "range"
%token  INTEGERTOKEN "integer"
%token  STRINGTYPETOKEN "string"
%token  BOOLEANTOKEN "boolean"
%token  LISTTOKEN "list"
%token  OFTOKEN "of"

%token  VARTOKEN "var"
%token  PROCTOKEN "proc"
%token  PROCEDURETOKEN "procedure"
%token  RETURNTOKEN "return"
%token  NOPTOKEN "nop"

%token  HELPTOKEN "help"
%token  VERSIONTOKEN "version"


%type <other> startsymbol;
%type <other> help;
%type <other> helpmeta;
%type <other> egalquestionmark;
%type <count> unaryplusminus;
%type <tree>  command;
%type <tree>  procbody;
%type <tree>  variabledeclaration;
%type <tree>  simplecommand;
%type <list>  commandlist;
%type <list>  variabledeclarationlist;
%type <list>  identifierlist;
%type <tree>  thing;
%type <tree>  supermegaterm;
%type <list>  thinglist;
%type <list>  matchlist;
%type <tree>  matchelement; 
%type <list>  structelementlist;
%type <association>  structelement;
%type <other>  structelementseparator;
%type <tree>  structuring;
%type <tree>  ifcommand;
%type <tree>  forcommand;
%type <tree>  assignment;
%type <tree>  simpleassignment;
%type <tree>  stateassignment;
%type <tree>  stillstateassignment;
%type <tree>  basicthing;
%type <tree>  list;
%type <tree>  constant;
%type <list>  simplelist;
%type <tree>  range;
%type <tree>  debound;
%type <tree>  headfunction;
%type <tree>  term;
%type <tree>  hyperterm;
%type <tree>  subterm;
%type <tree>  megaterm;
%type <tree>  statedereference;
%type <dblnode>  indexing;
%type <integerval> externalproctype;
%type <integerval> extendedexternalproctype;
%type <list>  externalproctypesimplelist;
%type <list>  externalproctypelist;
%type <other> beginsymbol;
%type <other> endsymbol;

%%

startsymbol:            command SEMICOLONTOKEN
                          {
			    parsedThing = $1;
			    $$ = NULL;
			    YYACCEPT;
			  }
                      | helpmeta SEMICOLONTOKEN
                          {
			    outputMode();
                            sollyaPrintf("This is %s.\nType 'help help;' for the list of available commands. Type 'help <command>;' for help on the specific command <command>.\nType 'quit;' for quitting the %s interpreter.\n\nYou can get moral support and help with bugs by writing to %s.\n\n",PACKAGE_NAME,PACKAGE_NAME,PACKAGE_BUGREPORT);
			    parsedThing = NULL;
			    $$ = NULL;
			    YYACCEPT;
			  }
                      | QUESTIONMARKTOKEN
                          {
			    outputMode();
                            sollyaPrintf("This is %s.\nType 'help help;' for the list of available commands. Type 'help <command>;' for help on the specific command <command>.\nType 'quit;' for quitting the %s interpreter.\n\nYou can get moral support and help with bugs by writing to %s.\n\n",PACKAGE_NAME,PACKAGE_NAME,PACKAGE_BUGREPORT);
			    parsedThing = NULL;
			    $$ = NULL;
			    YYACCEPT;
			  }
                      | helpmeta help SEMICOLONTOKEN
                          {
			    parsedThing = NULL;
			    $$ = NULL;
			    YYACCEPT;
			  }
                      | VERSIONTOKEN SEMICOLONTOKEN
                          {
			    outputMode();
			    sollyaPrintf("This is\n\n\t%s.\n\n"	VERSION_COPYRIGHT_TEXT "\nSend bug reports to <%s>\n\nThis build of %s is based on GMP %s, MPFR %s and MPFI %s.\n",PACKAGE_STRING,PACKAGE_BUGREPORT,PACKAGE_STRING,gmp_version,mpfr_get_version(),sollya_mpfi_get_version());
#if defined(HAVE_FPLLL_VERSION_STRING)
			    sollyaPrintf("It uses FPLLL as: \"%s\"\n",HAVE_FPLLL_VERSION_STRING);
#endif
			    sollyaPrintf("\n");
			    parsedThing = NULL;
			    $$ = NULL;
			    YYACCEPT;
			  }
                      | error SEMICOLONTOKEN
                          {
			    parsedThing = NULL;
			    $$ = NULL;
			    YYACCEPT;
			  }
;

helpmeta:               HELPTOKEN
                          {
			    helpNotFinished = 1;
			    $$ = NULL;
			  }
;

beginsymbol:            BEGINTOKEN
                          {
			    $$ = NULL;
			  }
                      | LEFTCURLYBRACETOKEN
		          {
			    $$ = NULL;
			  }
;

endsymbol:              ENDTOKEN
                          {
			    $$ = NULL;
			  }
                      | RIGHTCURLYBRACETOKEN
		          {
			    $$ = NULL;
			  }
;

command:                simplecommand
                          {
			    $$ = $1;
			  }
                      | beginsymbol commandlist endsymbol
                          {
			    $$ = makeCommandList($2);
                          }
                      | beginsymbol variabledeclarationlist commandlist endsymbol
                          {
			    $$ = makeCommandList(concatChains($2, $3));
                          }
                      | beginsymbol variabledeclarationlist endsymbol
                          {
			    $$ = makeCommandList($2);
                          }
                      | beginsymbol endsymbol
                          {
			    $$ = makeNop();
                          }
                      | IFTOKEN ifcommand
                          {
			    $$ = $2;
			  }
                      | WHILETOKEN thing DOTOKEN command
                          {
			    $$ = makeWhile($2, $4);
			  }
                      | FORTOKEN forcommand
                          {
			    $$ = $2;
			  }
;

ifcommand:              thing THENTOKEN command
                          {
			    $$ = makeIf($1, $3);
                          }
                      | thing THENTOKEN command ELSETOKEN command
                          {
			    $$ = makeIfElse($1,$3,$5);
                          }
;



forcommand:             IDENTIFIERTOKEN FROMTOKEN thing TOTOKEN thing DOTOKEN command
                          {
			    $$ = makeFor($1, $3, $5, makeConstantDouble(1.0), $7);
			    safeFree($1);
                          }
                      | IDENTIFIERTOKEN FROMTOKEN thing TOTOKEN thing BYTOKEN thing DOTOKEN command
                          {
			    $$ = makeFor($1, $3, $5, $7, $9);
			    safeFree($1);
                          }
                      | IDENTIFIERTOKEN INTOKEN thing DOTOKEN command
                          {
			    $$ = makeForIn($1, $3, $5);
			    safeFree($1);
                          }
;


commandlist:            command SEMICOLONTOKEN
                          {
			    $$ = addElement(NULL, $1);
			  }
                      | command SEMICOLONTOKEN commandlist
                          {
			    $$ = addElement($3, $1);
			  }
;

variabledeclarationlist: variabledeclaration SEMICOLONTOKEN
                          {
			    $$ = addElement(NULL, $1);
			  }
                      | variabledeclaration SEMICOLONTOKEN variabledeclarationlist
                          {
			    $$ = addElement($3, $1);
			  }
;

variabledeclaration:    VARTOKEN identifierlist
                          {
			    $$ = makeVariableDeclaration($2);
			  }
;


identifierlist:         IDENTIFIERTOKEN
                          {
			    $$ = addElement(NULL, $1);
			  }
                      | IDENTIFIERTOKEN COMMATOKEN identifierlist
                          {
			    $$ = addElement($3, $1);
			  }
;

procbody:               LPARTOKEN RPARTOKEN beginsymbol commandlist endsymbol
                          {
			    $$ = makeProc(NULL, makeCommandList($4), makeUnit());
                          }
                      | LPARTOKEN RPARTOKEN beginsymbol variabledeclarationlist commandlist endsymbol
                          {
			    $$ = makeProc(NULL, makeCommandList(concatChains($4, $5)), makeUnit());
                          }
                      | LPARTOKEN RPARTOKEN beginsymbol variabledeclarationlist endsymbol
                          {
			    $$ = makeProc(NULL, makeCommandList($4), makeUnit());
                          }
                      | LPARTOKEN RPARTOKEN beginsymbol endsymbol
                          {
			    $$ = makeProc(NULL, makeCommandList(addElement(NULL,makeNop())), makeUnit());
                          }
                      | LPARTOKEN RPARTOKEN beginsymbol commandlist RETURNTOKEN thing SEMICOLONTOKEN endsymbol
                          {
			    $$ = makeProc(NULL, makeCommandList($4), $6);
                          }
                      | LPARTOKEN RPARTOKEN beginsymbol variabledeclarationlist commandlist RETURNTOKEN thing SEMICOLONTOKEN endsymbol
                          {
			    $$ = makeProc(NULL, makeCommandList(concatChains($4, $5)), $7);
                          }
                      | LPARTOKEN RPARTOKEN beginsymbol variabledeclarationlist RETURNTOKEN thing SEMICOLONTOKEN endsymbol
                          {
			    $$ = makeProc(NULL, makeCommandList($4), $6);
                          }
                      | LPARTOKEN RPARTOKEN beginsymbol RETURNTOKEN thing SEMICOLONTOKEN endsymbol
                          {
			    $$ = makeProc(NULL, makeCommandList(addElement(NULL,makeNop())), $5);
                          }
                      | LPARTOKEN identifierlist RPARTOKEN beginsymbol commandlist endsymbol
                          {
			    $$ = makeProc($2, makeCommandList($5), makeUnit());
                          }
                      | LPARTOKEN identifierlist RPARTOKEN beginsymbol variabledeclarationlist commandlist endsymbol
                          {
			    $$ = makeProc($2, makeCommandList(concatChains($5, $6)), makeUnit());
                          }
                      | LPARTOKEN identifierlist RPARTOKEN beginsymbol variabledeclarationlist endsymbol
                          {
			    $$ = makeProc($2, makeCommandList($5), makeUnit());
                          }
                      | LPARTOKEN identifierlist RPARTOKEN beginsymbol endsymbol
                          {
			    $$ = makeProc($2, makeCommandList(addElement(NULL,makeNop())), makeUnit());
                          }
                      | LPARTOKEN identifierlist RPARTOKEN beginsymbol commandlist RETURNTOKEN thing SEMICOLONTOKEN endsymbol
                          {
			    $$ = makeProc($2, makeCommandList($5), $7);
                          }
                      | LPARTOKEN identifierlist RPARTOKEN beginsymbol variabledeclarationlist commandlist RETURNTOKEN thing SEMICOLONTOKEN endsymbol
                          {
			    $$ = makeProc($2, makeCommandList(concatChains($5, $6)), $8);
                          }
                      | LPARTOKEN identifierlist RPARTOKEN beginsymbol variabledeclarationlist RETURNTOKEN thing SEMICOLONTOKEN endsymbol
                          {
			    $$ = makeProc($2, makeCommandList($5), $7);
                          }
                      | LPARTOKEN identifierlist RPARTOKEN beginsymbol RETURNTOKEN thing SEMICOLONTOKEN endsymbol
                          {
			    $$ = makeProc($2, makeCommandList(addElement(NULL, makeNop())), $6);
                          }
                      | LPARTOKEN IDENTIFIERTOKEN EQUALTOKEN DOTSTOKEN RPARTOKEN beginsymbol commandlist endsymbol
                          {
			    $$ = makeProcIllim($2, makeCommandList($7), makeUnit());
                          }
                      | LPARTOKEN IDENTIFIERTOKEN EQUALTOKEN DOTSTOKEN RPARTOKEN beginsymbol variabledeclarationlist commandlist endsymbol
                          {
			    $$ = makeProcIllim($2, makeCommandList(concatChains($7, $8)), makeUnit());
                          }
                      | LPARTOKEN IDENTIFIERTOKEN EQUALTOKEN DOTSTOKEN RPARTOKEN beginsymbol variabledeclarationlist endsymbol
                          {
			    $$ = makeProcIllim($2, makeCommandList($7), makeUnit());
                          }
                      | LPARTOKEN IDENTIFIERTOKEN EQUALTOKEN DOTSTOKEN RPARTOKEN beginsymbol endsymbol
                          {
			    $$ = makeProcIllim($2, makeCommandList(addElement(NULL,makeNop())), makeUnit());
                          }
                      | LPARTOKEN IDENTIFIERTOKEN EQUALTOKEN DOTSTOKEN RPARTOKEN beginsymbol commandlist RETURNTOKEN thing SEMICOLONTOKEN endsymbol
                          {
			    $$ = makeProcIllim($2, makeCommandList($7), $9);
                          }
                      | LPARTOKEN IDENTIFIERTOKEN EQUALTOKEN DOTSTOKEN RPARTOKEN beginsymbol variabledeclarationlist commandlist RETURNTOKEN thing SEMICOLONTOKEN endsymbol
                          {
			    $$ = makeProcIllim($2, makeCommandList(concatChains($7, $8)), $10);
                          }
                      | LPARTOKEN IDENTIFIERTOKEN EQUALTOKEN DOTSTOKEN RPARTOKEN beginsymbol variabledeclarationlist RETURNTOKEN thing SEMICOLONTOKEN endsymbol
                          {
			    $$ = makeProcIllim($2, makeCommandList($7), $9);
                          }
                      | LPARTOKEN IDENTIFIERTOKEN EQUALTOKEN DOTSTOKEN RPARTOKEN beginsymbol RETURNTOKEN thing SEMICOLONTOKEN endsymbol
                          {
			    $$ = makeProcIllim($2, makeCommandList(addElement(NULL, makeNop())), $8);
                          }
;


simplecommand:          QUITTOKEN
                          {
			    $$ = makeQuit();
			  }
                      | FALSEQUITTOKEN
                          {
			    $$ = makeFalseQuit();
			  }
                      | NOPTOKEN
                          {
			    $$ = makeNop();
			  }
                      | NOPTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeNopArg($3);
			  }
                      | NOPTOKEN LPARTOKEN RPARTOKEN
                          {
			    $$ = makeNopArg(makeDefault());
			  }
                      | RESTARTTOKEN
                          {
			    $$ = makeRestart();
			  }
                      | PRINTTOKEN LPARTOKEN thinglist RPARTOKEN
                          {
			    $$ = makePrint($3);
			  }
                      | PRINTTOKEN LPARTOKEN thinglist RPARTOKEN RIGHTANGLETOKEN thing
                          {
			    $$ = makeNewFilePrint($6, $3);
			  }
                      | PRINTTOKEN LPARTOKEN thinglist RPARTOKEN RIGHTANGLETOKEN RIGHTANGLETOKEN thing
                          {
			    $$ = makeAppendFilePrint($7, $3);
			  }
                      | PLOTTOKEN LPARTOKEN thing COMMATOKEN thinglist RPARTOKEN
                          {
			    $$ = makePlot(addElement($5, $3));
			  }
                      | PRINTHEXATOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makePrintHexa($3);
			  }
                      | PRINTFLOATTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makePrintFloat($3);
			  }
                      | PRINTBINARYTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makePrintBinary($3);
			  }
                      | SUPPRESSMESSAGETOKEN LPARTOKEN thinglist RPARTOKEN
                          {
			    $$ = makeSuppressMessage($3);
			  }
                      | UNSUPPRESSMESSAGETOKEN LPARTOKEN thinglist RPARTOKEN
                          {
			    $$ = makeUnsuppressMessage($3);
			  }
                      | PRINTEXPANSIONTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makePrintExpansion($3);
			  }
                      | IMPLEMENTCONSTTOKEN LPARTOKEN thinglist RPARTOKEN
                          {
			    $$ = makeImplementConst($3);
			  }
                      | BASHEXECUTETOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeBashExecute($3);
			  }
                      | EXTERNALPLOTTOKEN LPARTOKEN thing COMMATOKEN thing COMMATOKEN thing COMMATOKEN thing COMMATOKEN thinglist RPARTOKEN
                          {
			    $$ = makeExternalPlot(addElement(addElement(addElement(addElement($11,$9),$7),$5),$3));
			  }
                      | WRITETOKEN LPARTOKEN thinglist RPARTOKEN
                          {
			    $$ = makeWrite($3);
			  }
                      | WRITETOKEN LPARTOKEN thinglist RPARTOKEN RIGHTANGLETOKEN thing
                          {
			    $$ = makeNewFileWrite($6, $3);
			  }
                      | WRITETOKEN LPARTOKEN thinglist RPARTOKEN RIGHTANGLETOKEN RIGHTANGLETOKEN thing
                          {
			    $$ = makeAppendFileWrite($7, $3);
			  }
                      | ASCIIPLOTTOKEN LPARTOKEN thing COMMATOKEN thing RPARTOKEN
                          {
			    $$ = makeAsciiPlot($3, $5);
			  }
                      | PRINTXMLTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makePrintXml($3);
			  }
                      | EXECUTETOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeExecute($3);
			  }
                      | PRINTXMLTOKEN LPARTOKEN thing RPARTOKEN RIGHTANGLETOKEN thing
                          {
			    $$ = makePrintXmlNewFile($3,$6);
			  }
                      | PRINTXMLTOKEN LPARTOKEN thing RPARTOKEN RIGHTANGLETOKEN RIGHTANGLETOKEN thing
                          {
			    $$ = makePrintXmlAppendFile($3,$7);
			  }
                      | WORSTCASETOKEN LPARTOKEN thing COMMATOKEN thing COMMATOKEN thing COMMATOKEN thing COMMATOKEN thinglist RPARTOKEN
                          {
			    $$ = makeWorstCase(addElement(addElement(addElement(addElement($11, $9), $7), $5), $3));
			  }
                      | RENAMETOKEN LPARTOKEN IDENTIFIERTOKEN COMMATOKEN IDENTIFIERTOKEN RPARTOKEN
                          {
			    $$ = makeRename($3, $5);
			    safeFree($3);
			    safeFree($5);
			  }
                      | RENAMETOKEN LPARTOKEN IDENTIFIERTOKEN COMMATOKEN FREEVARTOKEN RPARTOKEN
                          {
			    $$ = makeRename($3, "_x_");
			    safeFree($3);
			  }
                      | RENAMETOKEN LPARTOKEN FREEVARTOKEN COMMATOKEN IDENTIFIERTOKEN RPARTOKEN
                          {
			    $$ = makeRename("_x_", $5);
			    safeFree($5);
			  }
                      | RENAMETOKEN LPARTOKEN FREEVARTOKEN COMMATOKEN FREEVARTOKEN RPARTOKEN
                          {
			    $$ = makeRename("_x_", "_x_");
			  }
                      | EXTERNALPROCTOKEN LPARTOKEN IDENTIFIERTOKEN COMMATOKEN thing COMMATOKEN externalproctypelist MINUSTOKEN RIGHTANGLETOKEN extendedexternalproctype RPARTOKEN
                          {
			    $$ = makeExternalProc($3, $5, addElement($7, $10));
			    safeFree($3);
			  }
                      | EXTERNALDATATOKEN LPARTOKEN IDENTIFIERTOKEN COMMATOKEN thing RPARTOKEN
                          {
			    $$ = makeExternalData($3, $5);
			    safeFree($3);
			  }
                      | assignment
                          {
			    $$ = $1;
			  }
                      | thinglist
                          {
			    $$ = makeAutoprint($1);
			  }
                      | PROCEDURETOKEN IDENTIFIERTOKEN procbody
                          {
			    $$ = makeAssignment($2, $3);
			    safeFree($2);
			  }
;

assignment:             stateassignment
                          {
			    $$ = $1;
			  }
                      | stillstateassignment EXCLAMATIONTOKEN
                          {
			    $$ = $1;
			  }
                      | simpleassignment
                          {
			    $$ = $1;
			  }
                      | simpleassignment EXCLAMATIONTOKEN
                          {
			    $$ = $1;
			  }
;

simpleassignment:       IDENTIFIERTOKEN EQUALTOKEN thing
                          {
			    $$ = makeAssignment($1, $3);
			    safeFree($1);
			  }
                      | IDENTIFIERTOKEN ASSIGNEQUALTOKEN thing
                          {
			    $$ = makeFloatAssignment($1, $3);
			    safeFree($1);
			  }
                      | IDENTIFIERTOKEN EQUALTOKEN LIBRARYTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeLibraryBinding($1, $5);
			    safeFree($1);
			  }
                      | IDENTIFIERTOKEN EQUALTOKEN LIBRARYCONSTANTTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeLibraryConstantBinding($1, $5);
			    safeFree($1);
			  }
                      | indexing EQUALTOKEN thing
                          {
			    $$ = makeAssignmentInIndexing($1->a,$1->b,$3);
			    safeFree($1);
			  }
                      | indexing ASSIGNEQUALTOKEN thing
                          {
			    $$ = makeFloatAssignmentInIndexing($1->a,$1->b,$3);
			    safeFree($1);
			  }
                      | structuring EQUALTOKEN thing
                          {
			    $$ = makeProtoAssignmentInStructure($1,$3);
			  }
                      | structuring ASSIGNEQUALTOKEN thing
                          {
			    $$ = makeProtoFloatAssignmentInStructure($1,$3);
			  }
;

structuring:            basicthing DOTTOKEN IDENTIFIERTOKEN 
		          {
			    $$ = makeStructAccess($1,$3);
			    safeFree($3);
			  }
;

stateassignment:        PRECTOKEN EQUALTOKEN thing
                          {
			    $$ = makePrecAssign($3);
			  }
                      | POINTSTOKEN EQUALTOKEN thing
                          {
			    $$ = makePointsAssign($3);
			  }
                      | DIAMTOKEN EQUALTOKEN thing
                          {
			    $$ = makeDiamAssign($3);
			  }
                      | DISPLAYTOKEN EQUALTOKEN thing
                          {
			    $$ = makeDisplayAssign($3);
			  }
                      | VERBOSITYTOKEN EQUALTOKEN thing
                          {
			    $$ = makeVerbosityAssign($3);
			  }
                      | SHOWMESSAGENUMBERSTOKEN EQUALTOKEN thing
                          {
			    $$ = makeShowMessageNumbersAssign($3);
			  }
                      | CANONICALTOKEN EQUALTOKEN thing
                          {
			    $$ = makeCanonicalAssign($3);
			  }
                      | AUTOSIMPLIFYTOKEN EQUALTOKEN thing
                          {
			    $$ = makeAutoSimplifyAssign($3);
			  }
                      | TAYLORRECURSIONSTOKEN EQUALTOKEN thing
                          {
			    $$ = makeTaylorRecursAssign($3);
			  }
                      | TIMINGTOKEN EQUALTOKEN thing
                          {
			    $$ = makeTimingAssign($3);
			  }
                      | FULLPARENTHESESTOKEN EQUALTOKEN thing
                          {
			    $$ = makeFullParenAssign($3);
			  }
                      | MIDPOINTMODETOKEN EQUALTOKEN thing
                          {
			    $$ = makeMidpointAssign($3);
			  }
                      | DIEONERRORMODETOKEN EQUALTOKEN thing
                          {
			    $$ = makeDieOnErrorAssign($3);
			  }
                      | RATIONALMODETOKEN EQUALTOKEN thing
                          {
			    $$ = makeRationalModeAssign($3);
			  }
                      | SUPPRESSWARNINGSTOKEN EQUALTOKEN thing
                          {
			    $$ = makeSuppressWarningsAssign($3);
			  }
                      | HOPITALRECURSIONSTOKEN EQUALTOKEN thing
                          {
			    $$ = makeHopitalRecursAssign($3);
			  }
;

stillstateassignment:   PRECTOKEN EQUALTOKEN thing
                          {
			    $$ = makePrecStillAssign($3);
			  }
                      | POINTSTOKEN EQUALTOKEN thing
                          {
			    $$ = makePointsStillAssign($3);
			  }
                      | DIAMTOKEN EQUALTOKEN thing
                          {
			    $$ = makeDiamStillAssign($3);
			  }
                      | DISPLAYTOKEN EQUALTOKEN thing
                          {
			    $$ = makeDisplayStillAssign($3);
			  }
                      | VERBOSITYTOKEN EQUALTOKEN thing
                          {
			    $$ = makeVerbosityStillAssign($3);
			  }
                      | SHOWMESSAGENUMBERSTOKEN EQUALTOKEN thing
                          {
			    $$ = makeShowMessageNumbersStillAssign($3);
			  }
                      | CANONICALTOKEN EQUALTOKEN thing
                          {
			    $$ = makeCanonicalStillAssign($3);
			  }
                      | AUTOSIMPLIFYTOKEN EQUALTOKEN thing
                          {
			    $$ = makeAutoSimplifyStillAssign($3);
			  }
                      | TAYLORRECURSIONSTOKEN EQUALTOKEN thing
                          {
			    $$ = makeTaylorRecursStillAssign($3);
			  }
                      | TIMINGTOKEN EQUALTOKEN thing
                          {
			    $$ = makeTimingStillAssign($3);
			  }
                      | FULLPARENTHESESTOKEN EQUALTOKEN thing
                          {
			    $$ = makeFullParenStillAssign($3);
			  }
                      | MIDPOINTMODETOKEN EQUALTOKEN thing
                          {
			    $$ = makeMidpointStillAssign($3);
			  }
                      | DIEONERRORMODETOKEN EQUALTOKEN thing
                          {
			    $$ = makeDieOnErrorStillAssign($3);
			  }
                      | RATIONALMODETOKEN EQUALTOKEN thing
                          {
			    $$ = makeRationalModeStillAssign($3);
			  }
                      | SUPPRESSWARNINGSTOKEN EQUALTOKEN thing
                          {
			    $$ = makeSuppressWarningsStillAssign($3);
			  }
                      | HOPITALRECURSIONSTOKEN EQUALTOKEN thing
                          {
			    $$ = makeHopitalRecursStillAssign($3);
			  }
;

thinglist:              thing
                          {
			    $$ = addElement(NULL, $1);
			  }
                      | thing COMMATOKEN thinglist
                          {
			    $$ = addElement($3, $1);
			  }
;

structelementlist:      structelement
                          {
			    $$ = addElement(NULL, $1);
			  }
                      | structelement structelementseparator structelementlist
                          {
			    $$ = addElement($3, $1);
			  }
;

structelementseparator: COMMATOKEN
                          {
			    $$ = NULL;
			  }
                      | SEMICOLONTOKEN
		          {
			    $$ = NULL;
			  }
;

structelement:          DOTTOKEN IDENTIFIERTOKEN EQUALTOKEN thing
                          {
			    $$ = (entry *) safeMalloc(sizeof(entry));
			    $$->name = (char *) safeCalloc(strlen($2) + 1, sizeof(char));
			    strcpy($$->name,$2);
			    safeFree($2);
			    $$->value = (void *) ($4);
			  }
;

thing:                  supermegaterm
                         {
			   $$ = $1;
			 }
                      | MATCHTOKEN supermegaterm WITHTOKEN matchlist
		          {
			    $$ = makeMatch($2,$4);
			  }
;

supermegaterm:          megaterm
                          {
			    $$ = $1;
			  }
                      | thing ANDTOKEN megaterm
                          {
			    $$ = makeAnd($1, $3);
			  }
                      | thing ORTOKEN megaterm
                          {
			    $$ = makeOr($1, $3);
			  }
                      | EXCLAMATIONTOKEN megaterm
                          {
			    $$ = makeNegation($2);
			  }
;

indexing:               basicthing LBRACKETTOKEN thing RBRACKETTOKEN
                          {
			    $$ = (doubleNode *) safeMalloc(sizeof(doubleNode));
			    $$->a = $1;
			    $$->b = $3;
			  }
;


megaterm:               hyperterm
                          {
			    $$ = $1;
			  }
                      | megaterm COMPAREEQUALTOKEN hyperterm
                          {
			    $$ = makeCompareEqual($1, $3);
			  }
                      | megaterm INTOKEN hyperterm
                          {
			    $$ = makeCompareIn($1, $3);
			  }
                      | megaterm LEFTANGLETOKEN hyperterm
                          {
			    $$ = makeCompareLess($1, $3);
			  }
                      | megaterm RIGHTANGLETOKEN hyperterm
                          {
			    $$ = makeCompareGreater($1, $3);
			  }
                      | megaterm LEFTANGLETOKEN EQUALTOKEN hyperterm
                          {
			    $$ = makeCompareLessEqual($1, $4);
			  }
                      | megaterm RIGHTANGLETOKEN EQUALTOKEN hyperterm
                          {
			    $$ = makeCompareGreaterEqual($1, $4);
			  }
                      | megaterm EXCLAMATIONEQUALTOKEN hyperterm
                          {
			    $$ = makeCompareNotEqual($1, $3);
			  }
;

hyperterm:                term
                          {
			    $$ = $1;
			  }
                      | hyperterm PLUSTOKEN term
                          {
			    $$ = makeAdd($1, $3);
			  }
                      | hyperterm MINUSTOKEN term
                          {
			    $$ = makeSub($1, $3);
			  }
                      | hyperterm ATTOKEN term
                          {
			    $$ = makeConcat($1, $3);
			  }
                      | hyperterm DOUBLECOLONTOKEN term
                          {
			    $$ = makeAddToList($1, $3);
			  }
                      | hyperterm COLONDOTTOKEN term
                          {
			    $$ = makeAppend($1, $3);
			  }
;

unaryplusminus:         PLUSTOKEN
			  {
			    $$ = 0;
                          }
		      |	MINUSTOKEN
                          {
			    $$ = 1;
                          }
                      | PLUSTOKEN unaryplusminus
			  {
			    $$ = $2;
                          }
		      |	MINUSTOKEN unaryplusminus
                          {
			    $$ = $2+1;
                          }
;


term:                   subterm
			  {
			    $$ = $1;
                          }
		      |	unaryplusminus subterm
                          {
			    tempNode = $2;
			    for (tempInteger=0;tempInteger<$1;tempInteger++)
			      tempNode = makeNeg(tempNode);
			    $$ = tempNode;
                          }
		      |	APPROXTOKEN subterm
                          {
			    $$ = makeEvalConst($2);
                          }
		      |	term MULTOKEN subterm
			  {
			    $$ = makeMul($1, $3);
                          }
		      |	term DIVTOKEN subterm
                          {
			    $$ = makeDiv($1, $3);
                          }
		      |	term MULTOKEN unaryplusminus subterm
			  {
			    tempNode = $4;
			    for (tempInteger=0;tempInteger<$3;tempInteger++)
			      tempNode = makeNeg(tempNode);
			    $$ = makeMul($1, tempNode);
                          }
		      |	term DIVTOKEN unaryplusminus subterm
                          {
			    tempNode = $4;
			    for (tempInteger=0;tempInteger<$3;tempInteger++)
			      tempNode = makeNeg(tempNode);
			    $$ = makeDiv($1, tempNode);
                          }
		      |	term MULTOKEN APPROXTOKEN subterm
			  {
			    $$ = makeMul($1, makeEvalConst($4));
                          }
		      |	term DIVTOKEN APPROXTOKEN subterm
                          {
			    $$ = makeDiv($1, makeEvalConst($4));
                          }
;

subterm:                basicthing
                          {
			    $$ = $1;
                          }
                      | basicthing POWTOKEN subterm
                          {
			    $$ = makePow($1, $3);
                          }
                      | basicthing POWTOKEN unaryplusminus subterm
                          {
			    tempNode = $4;
			    for (tempInteger=0;tempInteger<$3;tempInteger++)
			      tempNode = makeNeg(tempNode);
			    $$ = makePow($1, tempNode);
                          }
                      | basicthing POWTOKEN APPROXTOKEN subterm
                          {
			    $$ = makePow($1, makeEvalConst($4));
                          }
                      | basicthing DOTCOLONTOKEN subterm
                          {
			    $$ = makePrepend($1, $3);
			  }
                      | basicthing DOTCOLONTOKEN APPROXTOKEN subterm
                          {
			    $$ = makePrepend($1, makeEvalConst($4));
			  }
;


basicthing:             ONTOKEN
                          {
			    $$ = makeOn();
			  }
                      | OFFTOKEN
                          {
			    $$ = makeOff();
			  }
                      | DYADICTOKEN
                          {
			    $$ = makeDyadic();
			  }
                      | POWERSTOKEN
                          {
			    $$ = makePowers();
			  }
                      | BINARYTOKEN
                          {
			    $$ = makeBinaryThing();
			  }
                      | HEXADECIMALTOKEN
                          {
			    $$ = makeHexadecimalThing();
			  }
                      | FILETOKEN
                          {
			    $$ = makeFile();
			  }
                      | POSTSCRIPTTOKEN
                          {
			    $$ = makePostscript();
			  }
                      | POSTSCRIPTFILETOKEN
                          {
			    $$ = makePostscriptFile();
			  }
                      | PERTURBTOKEN
                          {
			    $$ = makePerturb();
			  }
                      | MINUSWORDTOKEN
                          {
			    $$ = makeRoundDown();
			  }
                      | PLUSWORDTOKEN
                          {
			    $$ = makeRoundUp();
			  }
                      | ZEROWORDTOKEN
                          {
			    $$ = makeRoundToZero();
			  }
                      | NEARESTTOKEN
                          {
			    $$ = makeRoundToNearest();
			  }
                      | HONORCOEFFPRECTOKEN
                          {
			    $$ = makeHonorCoeff();
			  }
                      | TRUETOKEN
                          {
			    $$ = makeTrue();
			  }
                      | VOIDTOKEN
                          {
			    $$ = makeUnit();
			  }
                      | FALSETOKEN
                          {
			    $$ = makeFalse();
			  }
                      | DEFAULTTOKEN
                          {
			    $$ = makeDefault();
			  }
                      | DECIMALTOKEN
                          {
			    $$ = makeDecimal();
			  }
                      | ABSOLUTETOKEN
                          {
			    $$ = makeAbsolute();
			  }
                      | RELATIVETOKEN
                          {
			    $$ = makeRelative();
			  }
                      | FIXEDTOKEN
                          {
			    $$ = makeFixed();
			  }
                      | FLOATINGTOKEN
                          {
			    $$ = makeFloating();
			  }
                      | ERRORTOKEN
                          {
			    $$ = makeError();
			  }
                      | DOUBLETOKEN
                          {
			    $$ = makeDoubleSymbol();
			  }
                      | SINGLETOKEN
                          {
			    $$ = makeSingleSymbol();
			  }
                      | QUADTOKEN
                          {
			    $$ = makeQuadSymbol();
			  }
                      | HALFPRECISIONTOKEN
                          {
			    $$ = makeHalfPrecisionSymbol();
			  }
                      | DOUBLEEXTENDEDTOKEN
                          {
			    $$ = makeDoubleextendedSymbol();
			  }
                      | FREEVARTOKEN
                          {
			    $$ = makeVariable();
			  }
                      | DOUBLEDOUBLETOKEN
                          {
			    $$ = makeDoubleDoubleSymbol();
			  }
                      | TRIPLEDOUBLETOKEN
                          {
			    $$ = makeTripleDoubleSymbol();
			  }
                      | STRINGTOKEN
                          {
			    tempString = safeCalloc(strlen($1) + 1, sizeof(char));
			    strcpy(tempString, $1);
			    safeFree($1);
			    tempString2 = safeCalloc(strlen(tempString) + 1, sizeof(char));
			    strcpy(tempString2, tempString);
			    safeFree(tempString);
			    $$ = makeString(tempString2);
			    safeFree(tempString2);
			  }
                      | constant
                          {
			    $$ = $1;
			  }
                      | IDENTIFIERTOKEN
                          {
			    $$ = makeTableAccess($1);
			    safeFree($1);
			  }
                      | ISBOUNDTOKEN LPARTOKEN IDENTIFIERTOKEN RPARTOKEN
                          {
			    $$ = makeIsBound($3);
			    safeFree($3);
			  }
                      | IDENTIFIERTOKEN LPARTOKEN thinglist RPARTOKEN
                          {
			    $$ = makeTableAccessWithSubstitute($1, $3);
			    safeFree($1);
			  }
                      | IDENTIFIERTOKEN LPARTOKEN RPARTOKEN
                          {
			    $$ = makeTableAccessWithSubstitute($1, NULL);
			    safeFree($1);
			  }
                      | list
                          {
			    $$ = $1;
			  }
                      | range
                          {
			    $$ = $1;
			  }
                      | debound
                          {
			    $$ = $1;
			  }
                      | headfunction
                          {
			    $$ = $1;
			  }
                      | LPARTOKEN thing RPARTOKEN
                          {
			    $$ = $2;
			  }
                      | LEFTCURLYBRACETOKEN structelementlist RIGHTCURLYBRACETOKEN
		          {
			    $$ = makeStructure($2);
			  }
                      | statedereference
                          {
			    $$ = $1;
			  }
                      | indexing
                          {
			    $$ = makeIndex($1->a, $1->b);
			    safeFree($1);
			  }
                      | basicthing DOTTOKEN IDENTIFIERTOKEN 
		          {
			    $$ = makeStructAccess($1,$3);
			    safeFree($3);
			  }
                      | basicthing DOTTOKEN IDENTIFIERTOKEN LPARTOKEN RPARTOKEN
		          {
			    $$ = makeApply(makeStructAccess($1,$3),addElement(NULL, makeUnit()));
			    safeFree($3);
			  }
                      | basicthing DOTTOKEN IDENTIFIERTOKEN LPARTOKEN thinglist RPARTOKEN
		          {
			    $$ = makeApply(makeStructAccess($1,$3),$5);
			    safeFree($3);
			  }
                      | LPARTOKEN thing RPARTOKEN LPARTOKEN thinglist RPARTOKEN
                          {
			    $$ = makeApply($2,$5);
			  }
                      | LPARTOKEN thing RPARTOKEN LPARTOKEN RPARTOKEN
                          {
			    $$ = makeApply($2,addElement(NULL,makeUnit()));
			  }
                      | PROCTOKEN procbody
                          {
			    $$ = $2;
			  }
                      | TIMETOKEN LPARTOKEN command RPARTOKEN
                          {
			    $$ = makeTime($3);
                          }
;

matchlist:              matchelement
                          {
			    $$ = addElement(NULL,$1);
			  }
                      | matchelement matchlist
		          {
			    $$ = addElement($2,$1);
			  }
;

matchelement:          thing COLONTOKEN beginsymbol variabledeclarationlist commandlist RETURNTOKEN thing SEMICOLONTOKEN endsymbol
                          {
			    $$ = makeMatchElement($1,makeCommandList(concatChains($4, $5)),$7);
			  }
                      | thing COLONTOKEN beginsymbol variabledeclarationlist commandlist endsymbol
                          {
			    $$ = makeMatchElement($1,makeCommandList(concatChains($4, $5)),makeUnit());
			  }
                      | thing COLONTOKEN beginsymbol variabledeclarationlist RETURNTOKEN thing SEMICOLONTOKEN endsymbol
                          {
			    $$ = makeMatchElement($1,makeCommandList($4),$6);
			  }
                      | thing COLONTOKEN beginsymbol variabledeclarationlist endsymbol
                          {
			    $$ = makeMatchElement($1,makeCommandList($4),makeUnit());
			  }
                      | thing COLONTOKEN beginsymbol commandlist RETURNTOKEN thing SEMICOLONTOKEN endsymbol
                          {
			    $$ = makeMatchElement($1,makeCommandList($4),$6);
			  }
                      | thing COLONTOKEN beginsymbol commandlist endsymbol
                          {
			    $$ = makeMatchElement($1,makeCommandList($4),makeUnit());
			  }
                      | thing COLONTOKEN beginsymbol RETURNTOKEN thing SEMICOLONTOKEN endsymbol
                          {
			    $$ = makeMatchElement($1, makeCommandList(addElement(NULL,makeNop())), $5);
			  }
                      | thing COLONTOKEN beginsymbol endsymbol
                          {
			    $$ = makeMatchElement($1, makeCommandList(addElement(NULL,makeNop())), makeUnit());
			  }
                      | thing COLONTOKEN LPARTOKEN thing RPARTOKEN
		          {
			    $$ = makeMatchElement($1, makeCommandList(addElement(NULL,makeNop())), $4);
			  } 
;

constant:               CONSTANTTOKEN
                          {
			    $$ = makeDecimalConstant($1);
			    safeFree($1);
			  }
                      | MIDPOINTCONSTANTTOKEN
                          {
			    $$ = makeMidpointConstant($1);
			    safeFree($1);
			  }
                      | DYADICCONSTANTTOKEN
                          {
			    $$ = makeDyadicConstant($1);
			    safeFree($1);
			  }
                      | HEXCONSTANTTOKEN
                          {
			    $$ = makeHexConstant($1);
			    safeFree($1);
			  }
                      | HEXADECIMALCONSTANTTOKEN
                          {
			    $$ = makeHexadecimalConstant($1);
			    safeFree($1);
			  }
                      | BINARYCONSTANTTOKEN
                          {
			    $$ = makeBinaryConstant($1);
			    safeFree($1);
			  }
                      | PITOKEN
                          {
			    $$ = makePi();
			  }
;



list:                   LBRACKETTOKEN VERTBARTOKEN VERTBARTOKEN RBRACKETTOKEN
                          {
			    $$ = makeEmptyList();
			  }
                      | LBRACKETTOKEN ORTOKEN RBRACKETTOKEN
                          {
			    $$ = makeEmptyList();
			  }
		      | LBRACKETTOKEN VERTBARTOKEN simplelist VERTBARTOKEN RBRACKETTOKEN
                          {
			    $$ = makeRevertedList($3);
			  }
                      | LBRACKETTOKEN VERTBARTOKEN simplelist DOTSTOKEN VERTBARTOKEN RBRACKETTOKEN
                          {
			    $$ = makeRevertedFinalEllipticList($3);
			  }
;

simplelist:             thing
                          {
			    $$ = addElement(NULL, $1);
			  }
                      | simplelist COMMATOKEN thing
                          {
			    $$ = addElement($1, $3);
			  }
                      | simplelist COMMATOKEN DOTSTOKEN COMMATOKEN thing
                          {
			    $$ = addElement(addElement($1, makeElliptic()), $5);
			  }
;


range:                  LBRACKETTOKEN thing COMMATOKEN thing RBRACKETTOKEN
                          {
			    $$ = makeRange($2, $4);
			  }
                      | LBRACKETTOKEN thing SEMICOLONTOKEN thing RBRACKETTOKEN
                          {
			    $$ = makeRange($2, $4);
			  }
                      | LBRACKETTOKEN thing RBRACKETTOKEN
                          {
			    $$ = makeRange($2, copyThing($2));
			  }
;

debound:                STARLEFTANGLETOKEN thing RIGHTANGLESTARTOKEN
                          {
			    $$ = makeDeboundMax($2);
			  }
                      | STARLEFTANGLETOKEN thing RIGHTANGLEDOTTOKEN
                          {
			    $$ = makeDeboundMid($2);
			  }
                      | STARLEFTANGLETOKEN thing RIGHTANGLEUNDERSCORETOKEN
                          {
			    $$ = makeDeboundMin($2);
			  }
                      | SUPTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeDeboundMax($3);
			  }
                      | MIDTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeDeboundMid($3);
			  }
                      | INFTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeDeboundMin($3);
			  }
;

headfunction:           DIFFTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeDiff($3);
			  }
                      | DIRTYSIMPLIFYTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeDirtysimplify($3);
			  }
                      | BASHEVALUATETOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeBashevaluate(addElement(NULL,$3));
			  }
                      | GETSUPPRESSEDMESSAGESTOKEN LPARTOKEN RPARTOKEN
                          {
			    $$ = makeGetSuppressedMessages();
			  }
                      | GETBACKTRACETOKEN LPARTOKEN RPARTOKEN
                          {
			    $$ = makeGetBacktrace();
			  }
                      | BASHEVALUATETOKEN LPARTOKEN thing COMMATOKEN thing RPARTOKEN
                          {
			    $$ = makeBashevaluate(addElement(addElement(NULL,$5),$3));
			  }
                      | REMEZTOKEN LPARTOKEN thing COMMATOKEN thing COMMATOKEN thinglist RPARTOKEN
                          {
			    $$ = makeRemez(addElement(addElement($7, $5), $3));
			  }
                      | ANNOTATEFUNCTIONTOKEN LPARTOKEN thing COMMATOKEN thing COMMATOKEN thing COMMATOKEN thinglist RPARTOKEN
                          {
			    $$ = makeAnnotateFunction(addElement(addElement(addElement($9, $7), $5), $3));
			  }
                      | BINDTOKEN LPARTOKEN thing COMMATOKEN IDENTIFIERTOKEN COMMATOKEN thing RPARTOKEN
                          {
			    $$ = makeBind($3, $5, $7);
			    safeFree($5);
			  } 
                      | MINTOKEN LPARTOKEN thinglist RPARTOKEN
                          {
			    $$ = makeMin($3);
			  }
                      | MAXTOKEN LPARTOKEN thinglist RPARTOKEN
                          {
			    $$ = makeMax($3);
			  }
                      | FPMINIMAXTOKEN LPARTOKEN thing COMMATOKEN thing COMMATOKEN thing COMMATOKEN thinglist RPARTOKEN
                          {
			    $$ = makeFPminimax(addElement(addElement(addElement($9, $7), $5), $3));
			  }
                      | HORNERTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeHorner($3);
			  }
                      | CANONICALTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeCanonicalThing($3);
			  }
                      | EXPANDTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeExpand($3);
			  }
                      | SIMPLIFYSAFETOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeSimplifySafe($3);
			  }
                      | TAYLORTOKEN LPARTOKEN thing COMMATOKEN thing COMMATOKEN thing RPARTOKEN
                          {
			    $$ = makeTaylor($3, $5, $7);
			  }
                      | TAYLORFORMTOKEN LPARTOKEN thing COMMATOKEN thing COMMATOKEN thinglist RPARTOKEN
                          {
                            $$ = makeTaylorform(addElement(addElement($7, $5), $3));
			  }
                      | CHEBYSHEVFORMTOKEN LPARTOKEN thing COMMATOKEN thing COMMATOKEN thing RPARTOKEN
                          {
                            $$ = makeChebyshevform(addElement(addElement(addElement(NULL, $7), $5), $3));
			  }
                      | AUTODIFFTOKEN LPARTOKEN thing COMMATOKEN thing COMMATOKEN thing RPARTOKEN
                          {
                            $$ = makeAutodiff(addElement(addElement(addElement(NULL, $7), $5), $3));
			  }
                      | DEGREETOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeDegree($3);
			  }
                      | NUMERATORTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeNumerator($3);
			  }
                      | DENOMINATORTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeDenominator($3);
			  }
                      | SUBSTITUTETOKEN LPARTOKEN thing COMMATOKEN thing RPARTOKEN
                          {
			    $$ = makeSubstitute($3, $5);
			  }
                      | COMPOSEPOLYNOMIALSTOKEN LPARTOKEN thing COMMATOKEN thing RPARTOKEN
                          {
			    $$ = makeComposePolynomials($3, $5);
			  }
                      | BEZOUTTOKEN LPARTOKEN thing COMMATOKEN thing RPARTOKEN
                          {
			    $$ = makeBezout($3, $5);
			  }
                      | COEFFTOKEN LPARTOKEN thing COMMATOKEN thing RPARTOKEN
                          {
			    $$ = makeCoeff($3, $5);
			  }
                      | SUBPOLYTOKEN LPARTOKEN thing COMMATOKEN thing RPARTOKEN
                          {
			    $$ = makeSubpoly($3, $5);
			  }
                      | ROUNDCOEFFICIENTSTOKEN LPARTOKEN thing COMMATOKEN thing RPARTOKEN
                          {
			    $$ = makeRoundcoefficients($3, $5);
			  }
                      | RATIONALAPPROXTOKEN LPARTOKEN thing COMMATOKEN thing RPARTOKEN
                          {
			    $$ = makeRationalapprox($3, $5);
			  }
                      | ACCURATEINFNORMTOKEN LPARTOKEN thing COMMATOKEN thing COMMATOKEN thinglist RPARTOKEN
                          {
			    $$ = makeAccurateInfnorm(addElement(addElement($7, $5), $3));
			  }
                      | ROUNDTOFORMATTOKEN LPARTOKEN thing COMMATOKEN thing COMMATOKEN thing RPARTOKEN
                          {
			    $$ = makeRoundToFormat($3, $5, $7);
			  }
                      | EVALUATETOKEN LPARTOKEN thing COMMATOKEN thing RPARTOKEN
                          {
			    $$ = makeEvaluate($3, $5);
			  }
                      | PARSETOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeParse($3);
			  }
                      | READXMLTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeReadXml($3);
			  }
                      | INFNORMTOKEN LPARTOKEN thing COMMATOKEN thinglist RPARTOKEN
                          {
			    $$ = makeInfnorm(addElement($5, $3));
			  }
                      | SUPNORMTOKEN LPARTOKEN thing COMMATOKEN thing COMMATOKEN thing COMMATOKEN thing COMMATOKEN thing RPARTOKEN
                          {
			    $$ = makeSupnorm(addElement(addElement(addElement(addElement(addElement(NULL,$11),$9),$7),$5),$3));
			  }
                      | FINDZEROSTOKEN LPARTOKEN thing COMMATOKEN thing RPARTOKEN
                          {
			    $$ = makeFindZeros($3, $5);
			  }
                      | FPFINDZEROSTOKEN LPARTOKEN thing COMMATOKEN thing RPARTOKEN
                          {
			    $$ = makeFPFindZeros($3, $5);
			  }
                      | DIRTYINFNORMTOKEN LPARTOKEN thing COMMATOKEN thing RPARTOKEN
                          {
			    $$ = makeDirtyInfnorm($3, $5);
			  }
                      | GCDTOKEN LPARTOKEN thing COMMATOKEN thing RPARTOKEN
                          {
			    $$ = makeGcd($3, $5);
			  }
                      | EUCLDIVTOKEN LPARTOKEN thing COMMATOKEN thing RPARTOKEN
                          {
			    $$ = makeEuclDiv($3, $5);
			  }
                      | EUCLMODTOKEN LPARTOKEN thing COMMATOKEN thing RPARTOKEN
                          {
			    $$ = makeEuclMod($3, $5);
			  }
                      | NUMBERROOTSTOKEN LPARTOKEN thing COMMATOKEN thing RPARTOKEN
                          {
			    $$ = makeNumberRoots($3, $5);
			  }
                      | INTEGRALTOKEN LPARTOKEN thing COMMATOKEN thing RPARTOKEN
                          {
			    $$ = makeIntegral($3, $5);
			  }
                      | DIRTYINTEGRALTOKEN LPARTOKEN thing COMMATOKEN thing RPARTOKEN
                          {
			    $$ = makeDirtyIntegral($3, $5);
			  }
                      | IMPLEMENTPOLYTOKEN LPARTOKEN thing COMMATOKEN thing COMMATOKEN thing COMMATOKEN thing COMMATOKEN thing COMMATOKEN thinglist RPARTOKEN
                          {
			    $$ = makeImplementPoly(addElement(addElement(addElement(addElement(addElement($13, $11), $9), $7), $5), $3));
			  }
                      | INTERPOLATETOKEN LPARTOKEN thing COMMATOKEN thinglist RPARTOKEN
                          {
			    $$ = makeInterpolate(addElement($5, $3));
			  }
                      | CHECKINFNORMTOKEN LPARTOKEN thing COMMATOKEN thing COMMATOKEN thing RPARTOKEN
                          {
			    $$ = makeCheckInfnorm($3, $5, $7);
			  }
                      | ZERODENOMINATORSTOKEN LPARTOKEN thing COMMATOKEN thing RPARTOKEN
                          {
			    $$ = makeZeroDenominators($3, $5);
			  }
                      | ISEVALUABLETOKEN LPARTOKEN thing COMMATOKEN thing RPARTOKEN
                          {
			    $$ = makeIsEvaluable($3, $5);
			  }
                      | SEARCHGALTOKEN LPARTOKEN thinglist RPARTOKEN
                          {
			    $$ = makeSearchGal($3);
			  }
                      | GUESSDEGREETOKEN LPARTOKEN thing COMMATOKEN thing COMMATOKEN thinglist RPARTOKEN
                          {
			    $$ = makeGuessDegree(addElement(addElement($7, $5), $3));
			  }
                      | DIRTYFINDZEROSTOKEN LPARTOKEN thing COMMATOKEN thing RPARTOKEN
                          {
			    $$ = makeDirtyFindZeros($3, $5);
			  }
                      | HEADTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeHead($3);
			  }
                      | ROUNDCORRECTLYTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeRoundCorrectly($3);
			  }
                      | READFILETOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeReadFile($3);
			  }
                      | REVERTTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeRevert($3);
			  }
                      | SORTTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeSort($3);
			  }
                      | MANTISSATOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeMantissa($3);
			  }
                      | EXPONENTTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeExponent($3);
			  }
                      | PRECISIONTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makePrecision($3);
			  }
                      | TAILTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeTail($3);
			  }
                      | SQRTTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeSqrt($3);
			  }
                      | EXPTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeExp($3);
			  }
                      | FREEVARTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeApply(makeVariable(),addElement(NULL,$3));
			  }                      
                      | FUNCTIONTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeProcedureFunction($3);
			  }
                      | FUNCTIONTOKEN LPARTOKEN thing COMMATOKEN thing RPARTOKEN
                          {
			    $$ = makeSubstitute(makeProcedureFunction($3),$5);
			  }
                      | LOGTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeLog($3);
			  }
                      | LOG2TOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeLog2($3);
			  }
                      | LOG10TOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeLog10($3);
			  }
                      | SINTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeSin($3);
			  }
                      | COSTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeCos($3);
			  }
                      | TANTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeTan($3);
			  }
                      | ASINTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeAsin($3);
			  }
                      | ACOSTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeAcos($3);
			  }
                      | ATANTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeAtan($3);
			  }
                      | SINHTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeSinh($3);
			  }
                      | COSHTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeCosh($3);
			  }
                      | TANHTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeTanh($3);
			  }
                      | ASINHTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeAsinh($3);
			  }
                      | ACOSHTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeAcosh($3);
			  }
                      | ATANHTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeAtanh($3);
			  }
                      | ABSTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeAbs($3);
			  }
                      | ERFTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeErf($3);
			  }
                      | ERFCTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeErfc($3);
			  }
                      | LOG1PTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeLog1p($3);
			  }
                      | EXPM1TOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeExpm1($3);
			  }
                      | DOUBLETOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeDouble($3);
			  }
                      | SINGLETOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeSingle($3);
			  }
                      | QUADTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeQuad($3);
			  }
                      | HALFPRECISIONTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeHalfPrecision($3);
			  }
                      | DOUBLEDOUBLETOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeDoubledouble($3);
			  }
                      | TRIPLEDOUBLETOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeTripledouble($3);
			  }
                      | DOUBLEEXTENDEDTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeDoubleextended($3);
			  }
                      | CEILTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeCeil($3);
			  }
                      | FLOORTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeFloor($3);
			  }
                      | NEARESTINTTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeNearestInt($3);
			  }
                      | LENGTHTOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeLength($3);
			  }
                      | OBJECTNAMETOKEN LPARTOKEN thing RPARTOKEN
                          {
			    $$ = makeObjectName($3);
			  }
;

egalquestionmark:       EQUALTOKEN QUESTIONMARKTOKEN
                          {
			    $$ = NULL;
			  }
                      |
                          {
			    $$ = NULL;
			  }
;

statedereference:       PRECTOKEN egalquestionmark
                          {
			    $$ = makePrecDeref();
			  }
                      | POINTSTOKEN egalquestionmark
                          {
			    $$ = makePointsDeref();
			  }
                      | DIAMTOKEN egalquestionmark
                          {
			    $$ = makeDiamDeref();
			  }
                      | DISPLAYTOKEN egalquestionmark
                          {
			    $$ = makeDisplayDeref();
			  }
                      | VERBOSITYTOKEN egalquestionmark
                          {
			    $$ = makeVerbosityDeref();
			  }
                      | SHOWMESSAGENUMBERSTOKEN egalquestionmark
                          {
			    $$ = makeShowMessageNumbersDeref();
			  }
                      | CANONICALTOKEN egalquestionmark
                          {
			    $$ = makeCanonicalDeref();
			  }
                      | AUTOSIMPLIFYTOKEN egalquestionmark
                          {
			    $$ = makeAutoSimplifyDeref();
			  }
                      | TAYLORRECURSIONSTOKEN egalquestionmark
                          {
			    $$ = makeTaylorRecursDeref();
			  }
                      | TIMINGTOKEN egalquestionmark
                          {
			    $$ = makeTimingDeref();
			  }
                      | FULLPARENTHESESTOKEN egalquestionmark
                          {
			    $$ = makeFullParenDeref();
			  }
                      | MIDPOINTMODETOKEN egalquestionmark
                          {
			    $$ = makeMidpointDeref();
			  }
                      | DIEONERRORMODETOKEN egalquestionmark
                          {
			    $$ = makeDieOnErrorDeref();
			  }
                      | RATIONALMODETOKEN egalquestionmark
                          {
			    $$ = makeRationalModeDeref();
			  }
                      | SUPPRESSWARNINGSTOKEN egalquestionmark
                          {
			    $$ = makeSuppressWarningsDeref();
			  }
                      | HOPITALRECURSIONSTOKEN egalquestionmark
                          {
			    $$ = makeHopitalRecursDeref();
			  }
;

externalproctype:       CONSTANTTYPETOKEN
                          {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = CONSTANT_TYPE;
			    $$ = tempIntPtr;
			  }
                      | FUNCTIONTOKEN
                          {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = FUNCTION_TYPE;
			    $$ = tempIntPtr;
			  }
                      | OBJECTTOKEN
                          {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = OBJECT_TYPE;
			    $$ = tempIntPtr;
			  }
                      | RANGETOKEN
                          {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = RANGE_TYPE;
			    $$ = tempIntPtr;
			  }
                      | INTEGERTOKEN
                          {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = INTEGER_TYPE;
			    $$ = tempIntPtr;
			  }
                      | STRINGTYPETOKEN
                          {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = STRING_TYPE;
			    $$ = tempIntPtr;
			  }
                      | BOOLEANTOKEN
                          {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = BOOLEAN_TYPE;
			    $$ = tempIntPtr;
			  }
                      | LISTTOKEN OFTOKEN CONSTANTTYPETOKEN
                          {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = CONSTANT_LIST_TYPE;
			    $$ = tempIntPtr;
			  }
                      | LISTTOKEN OFTOKEN FUNCTIONTOKEN
                          {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = FUNCTION_LIST_TYPE;
			    $$ = tempIntPtr;
			  }
                      | LISTTOKEN OFTOKEN OBJECTTOKEN
                          {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = OBJECT_LIST_TYPE;
			    $$ = tempIntPtr;
			  }
                      | LISTTOKEN OFTOKEN RANGETOKEN
                          {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = RANGE_LIST_TYPE;
			    $$ = tempIntPtr;
			  }
                      | LISTTOKEN OFTOKEN INTEGERTOKEN
                          {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = INTEGER_LIST_TYPE;
			    $$ = tempIntPtr;
			  }
                      | LISTTOKEN OFTOKEN STRINGTYPETOKEN
                          {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = STRING_LIST_TYPE;
			    $$ = tempIntPtr;
			  }
                      | LISTTOKEN OFTOKEN BOOLEANTOKEN
                          {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = BOOLEAN_LIST_TYPE;
			    $$ = tempIntPtr;
			  }
;

extendedexternalproctype: VOIDTOKEN
                          {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = VOID_TYPE;
			    $$ = tempIntPtr;
			  }
                      | externalproctype
		          {
			    $$ = $1;
		          }
;


externalproctypesimplelist:   externalproctype
                          {
			    $$ = addElement(NULL, $1);
			  }
                      | externalproctype COMMATOKEN externalproctypesimplelist
                          {
			    $$ = addElement($3, $1);
			  }
;

externalproctypelist:       extendedexternalproctype
                          {
			    $$ = addElement(NULL, $1);
			  }
                      | LPARTOKEN externalproctypesimplelist RPARTOKEN
                          {
			    $$ = $2;
			  }
;


help:                   CONSTANTTOKEN
                          {
			    outputMode(); sollyaPrintf("\"%s\" is recognized as a base 10 constant.\n",$1);
			    safeFree($1);
			  }
                      | DYADICCONSTANTTOKEN
                          {
			    outputMode(); sollyaPrintf("\"%s\" is recognized as a dyadic number constant.\n",$1);
			    safeFree($1);
                          }
                      | HEXCONSTANTTOKEN
                          {
			    outputMode(); sollyaPrintf("\"%s\" is recognized as a double or single precision constant.\n",$1);
			    safeFree($1);
                          }
                      | HEXADECIMALCONSTANTTOKEN
                          {
			    outputMode(); sollyaPrintf("\"%s\" is recognized as a hexadecimal constant.\n",$1);
			    safeFree($1);
                          }
                      | BINARYCONSTANTTOKEN
                          {
			    outputMode(); sollyaPrintf("\"%s_2\" is recognized as a base 2 constant.\n",$1);
			    safeFree($1);
                          }
                      | PITOKEN
                          {
			    outputMode(); sollya_print_help_text("pi");
                          }
                      | IDENTIFIERTOKEN
                          {
			    outputMode(); sollyaPrintf("\"%s\" is an identifier.\n",$1);
			    safeFree($1);
                          }
                      | STRINGTOKEN
                          {
			    outputMode(); sollyaPrintf("\"%s\" is a string constant.\n",$1);
			    safeFree($1);
                          }
                      | LPARTOKEN
                          {
			    outputMode(); sollyaPrintf("Left parenthesis.\n");
                          }
                      | RPARTOKEN
                          {
			    outputMode(); sollyaPrintf("Right parenthesis.\n");
                          }
                      | LBRACKETTOKEN
                          {
			    outputMode(); sollyaPrintf("Left bracket - indicates a range.\n");
                          }
                      | RBRACKETTOKEN
                          {
			    outputMode(); sollyaPrintf("Right bracket - indicates a range.\n");
                          }
                      | LBRACKETTOKEN VERTBARTOKEN
                          {
			    outputMode(); sollyaPrintf("Left bracket-bar - indicates a list.\n");
                          }
                      | VERTBARTOKEN RBRACKETTOKEN
                          {
			    outputMode(); sollyaPrintf("Bar-right bracket - indicates a list.\n");
                          }
                      | EQUALTOKEN
                          {
			    outputMode(); sollya_print_help_text("=");
                          }
                      | ASSIGNEQUALTOKEN
                          {
			    outputMode(); sollya_print_help_text(":=");
                          }
                      | COMPAREEQUALTOKEN
                          {
			    outputMode(); sollya_print_help_text("==");
                          }
                      | COMMATOKEN
                          {
			    outputMode(); sollyaPrintf("Separator in lists, ranges or structures.\n");
                          }
                      | EXCLAMATIONTOKEN
                          {
			    outputMode(); sollya_print_help_text("!");
                          }
                      | STARLEFTANGLETOKEN
                          {
			    outputMode(); sollyaPrintf("Dereferences range bounds.\n");
                          }
                      | LEFTANGLETOKEN
                          {
			    outputMode(); sollya_print_help_text("<");
                          }
                      | LEFTANGLETOKEN EQUALTOKEN
                          {
			    outputMode(); sollya_print_help_text("<=");
                          }
                      | RIGHTANGLEUNDERSCORETOKEN
                          {
			    outputMode(); sollyaPrintf("Dereferences the lower range bound.\n");
                          }
                      | RIGHTANGLEDOTTOKEN
                          {
			    outputMode(); sollyaPrintf("Dereferences the mid-point of a range.\n");
                          }
                      | RIGHTANGLETOKEN EQUALTOKEN
                          {
			    outputMode(); sollya_print_help_text(">=");
			  }
                      | DOTTOKEN
                          {
			    outputMode(); sollyaPrintf("Accessing an element in a structured type.\n");
			  }
                      | RIGHTANGLESTARTOKEN
                          {
			    outputMode(); sollyaPrintf("Dereferences the upper range bound.\n");
                          }
                      | RIGHTANGLETOKEN
                          {
			    outputMode(); sollya_print_help_text(">");
                          }
                      | DOTSTOKEN
                          {
			    outputMode(); sollyaPrintf("Ellipsis.\n");
                          }
                      | QUESTIONMARKTOKEN
                          {
			    outputMode(); sollyaPrintf("Dereferences global environment variables.\n");
                          }
                      | VERTBARTOKEN
                          {
			    outputMode(); sollyaPrintf("Starts or ends a list.\n");
                          }
                      | ATTOKEN
                          {
			    outputMode(); sollya_print_help_text("@");
                          }
                      | DOUBLECOLONTOKEN
                          {
			    outputMode(); sollyaPrintf("a::b prepends a to list b or appends b to list a, preprending list a to list b if both are lists.\n");
                          }
                      | DOTCOLONTOKEN
                          {
			    outputMode(); sollya_print_help_text(".:");
                          }
                      | COLONDOTTOKEN
                          {
			    outputMode(); sollya_print_help_text(":.");
                          }
                      | EXCLAMATIONEQUALTOKEN
                          {
			    outputMode(); sollya_print_help_text("!=");
                          }
                      | ANDTOKEN
                          {
			    outputMode(); sollya_print_help_text("&&");
                          }
                      | ORTOKEN
                          {
			    outputMode(); sollya_print_help_text("||");
                          }
                      | PLUSTOKEN
                          {
			    outputMode(); sollya_print_help_text("+");
                          }
                      | MINUSTOKEN
                          {
			    outputMode(); sollya_print_help_text("-");
                          }
                      | APPROXTOKEN
                          {
			    outputMode(); sollya_print_help_text("~");
                          }
                      | MULTOKEN
                          {
			    outputMode(); sollya_print_help_text("*");
                          }
                      | DIVTOKEN
                          {
			    outputMode(); sollya_print_help_text("/");
                          }
                      | POWTOKEN
                          {
			    outputMode(); sollya_print_help_text("^");
                          }
                      | SQRTTOKEN
                          {
			    outputMode(); sollya_print_help_text("sqrt");
                          }
                      | EXPTOKEN
                          {
			    outputMode(); sollya_print_help_text("exp");
                          }
                      | FREEVARTOKEN
                          {
			    outputMode(); sollya_print_help_text("_x_");
                          }
                      | LOGTOKEN
                          {
			    outputMode(); sollya_print_help_text("log");
                          }
                      | LOG2TOKEN
                          {
			    outputMode(); sollya_print_help_text("log2");
                          }
                      | LOG10TOKEN
                          {
			    outputMode(); sollya_print_help_text("log10");
                          }
                      | SINTOKEN
                          {
			    outputMode(); sollya_print_help_text("sin");
                          }
                      | COSTOKEN
                          {
			    outputMode(); sollya_print_help_text("cos");
                          }
                      | TANTOKEN
                          {
			    outputMode(); sollya_print_help_text("tan");
                          }
                      | ASINTOKEN
                          {
			    outputMode(); sollya_print_help_text("asin");
                          }
                      | ACOSTOKEN
                          {
			    outputMode(); sollya_print_help_text("acos");
                          }
                      | ATANTOKEN
                          {
			    outputMode(); sollya_print_help_text("atan");
                          }
                      | SINHTOKEN
                          {
			    outputMode(); sollya_print_help_text("sinh");
                          }
                      | COSHTOKEN
                          {
			    outputMode(); sollya_print_help_text("cosh");
                          }
                      | TANHTOKEN
                          {
			    outputMode(); sollya_print_help_text("tanh");
                          }
                      | ASINHTOKEN
                          {
			    outputMode(); sollya_print_help_text("asinh");
                          }
                      | ACOSHTOKEN
                          {
			    outputMode(); sollya_print_help_text("acosh");
                          }
                      | ATANHTOKEN
                          {
			    outputMode(); sollya_print_help_text("atanh");
                          }
                      | ABSTOKEN
                          {
			    outputMode(); sollya_print_help_text("abs");
                          }
                      | ERFTOKEN
                          {
			    outputMode(); sollya_print_help_text("erf");
                          }
                      | ERFCTOKEN
                          {
			    outputMode(); sollya_print_help_text("erfc");
                          }
                      | LOG1PTOKEN
                          {
			    outputMode(); sollya_print_help_text("log1p");
                          }
                      | EXPM1TOKEN
                          {
			    outputMode(); sollya_print_help_text("expm1");
                          }
                      | DOUBLETOKEN
                          {
			    outputMode(); sollya_print_help_text("double");
                          }
                      | SINGLETOKEN
                          {
			    outputMode(); sollya_print_help_text("single");
                          }
                      | QUADTOKEN
                          {
			    outputMode(); sollya_print_help_text("quad");
                          }
                      | HALFPRECISIONTOKEN
                          {
			    outputMode(); sollya_print_help_text("halfprecision");
                          }
                      | DOUBLEDOUBLETOKEN
                          {
			    outputMode(); sollya_print_help_text("doubledouble");
                          }
                      | TRIPLEDOUBLETOKEN
                          {
			    outputMode(); sollya_print_help_text("tripledouble");
                          }
                      | DOUBLEEXTENDEDTOKEN
                          {
			    outputMode(); sollya_print_help_text("doubleextended");
                          }
                      | CEILTOKEN
                          {
			    outputMode(); sollya_print_help_text("ceil");
                          }
                      | FLOORTOKEN
                          {
			    outputMode(); sollya_print_help_text("floor");
                          }
                      | NEARESTINTTOKEN
                          {
			    outputMode(); sollya_print_help_text("nearestint");
                          }
                      | HEADTOKEN
                          {
			    outputMode(); sollya_print_help_text("head");
                          }
                      | ROUNDCORRECTLYTOKEN
                          {
			    outputMode(); sollya_print_help_text("roundcorrectly");
                          }
                      | READFILETOKEN
                          {
			    outputMode(); sollya_print_help_text("readfile");
                          }
                      | REVERTTOKEN
                          {
			    outputMode(); sollya_print_help_text("revert");
                          }
                      | SORTTOKEN
                          {
			    outputMode(); sollya_print_help_text("sort");
                          }
                      | TAILTOKEN
                          {
			    outputMode(); sollya_print_help_text("tail");
                          }
                      | PRECTOKEN
                          {
			    outputMode(); sollya_print_help_text("prec");
                          }
                      | POINTSTOKEN
                          {
			    outputMode(); sollya_print_help_text("points");
                          }
                      | DIAMTOKEN
                          {
			    outputMode(); sollya_print_help_text("diam");
                          }
                      | DISPLAYTOKEN
                          {
			    outputMode(); sollya_print_help_text("display");
                          }
                      | VERBOSITYTOKEN
                          {
			    outputMode(); sollya_print_help_text("verbosity");
                          }
                      | SHOWMESSAGENUMBERSTOKEN
                          {
			    outputMode(); sollya_print_help_text("showmessagenumbers");
                          }
                      | CANONICALTOKEN
                          {
			    outputMode(); sollya_print_help_text("canonical");
                          }
                      | AUTOSIMPLIFYTOKEN
                          {
			    outputMode(); sollya_print_help_text("autosimplify");
                          }
                      | TAYLORRECURSIONSTOKEN
                          {
			    outputMode(); sollya_print_help_text("taylorrecursions");
                          }
                      | TIMINGTOKEN
                          {
			    outputMode(); sollya_print_help_text("timing");
                          }
                      | TIMETOKEN
                          {
			    outputMode(); sollya_print_help_text("time");
                          }
                      | FULLPARENTHESESTOKEN
                          {
			    outputMode(); sollya_print_help_text("fullparentheses");
                          }
                      | MIDPOINTMODETOKEN
                          {
			    outputMode(); sollya_print_help_text("midpointmode");
                          }
                      | DIEONERRORMODETOKEN
                          {
			    outputMode(); sollya_print_help_text("dieonerrormode");
                          }
                      | RATIONALMODETOKEN
                          {
			    outputMode(); sollya_print_help_text("rationalmode");
                          }
                      | SUPPRESSWARNINGSTOKEN
                          {
			    outputMode(); sollya_print_help_text("roundingwarnings");
                          }
                      | HOPITALRECURSIONSTOKEN
                          {
			    outputMode(); sollya_print_help_text("hopitalrecursions");
                          }
                      | ONTOKEN
                          {
			    outputMode(); sollya_print_help_text("on");
                          }
                      | OFFTOKEN
                          {
			    outputMode(); sollya_print_help_text("off");
                          }
                      | DYADICTOKEN
                          {
			    outputMode(); sollya_print_help_text("dyadic");
                          }
                      | POWERSTOKEN
                          {
			    outputMode(); sollya_print_help_text("powers");
                          }
                      | BINARYTOKEN
                          {
			    outputMode(); sollya_print_help_text("binary");
                          }
                      | HEXADECIMALTOKEN
                          {
			    outputMode(); sollya_print_help_text("hexadecimal");
                          }
                      | FILETOKEN
                          {
			    outputMode(); sollya_print_help_text("file");
                          }
                      | POSTSCRIPTTOKEN
                          {
			    outputMode(); sollya_print_help_text("postscript");
                          }
                      | POSTSCRIPTFILETOKEN
                          {
			    outputMode(); sollya_print_help_text("postscriptfile");
                          }
                      | PERTURBTOKEN
                          {
			    outputMode(); sollya_print_help_text("perturb");
                          }
                      | MINUSWORDTOKEN
                          {
			    outputMode(); sollya_print_help_text("RD");
                          }
                      | PLUSWORDTOKEN
                          {
			    outputMode(); sollya_print_help_text("RU");
                          }
                      | ZEROWORDTOKEN
                          {
			    outputMode(); sollya_print_help_text("RZ");
                          }
                      | NEARESTTOKEN
                          {
			    outputMode(); sollya_print_help_text("RN");
                          }
                      | HONORCOEFFPRECTOKEN
                          {
			    outputMode(); sollya_print_help_text("honorcoeffprec");
                          }
                      | TRUETOKEN
                          {
			    outputMode(); sollya_print_help_text("true");
                          }
                      | FALSETOKEN
                          {
			    outputMode(); sollya_print_help_text("false");
                          }
                      | DEFAULTTOKEN
                          {
			    outputMode(); sollya_print_help_text("default");
                          }
                      | MATCHTOKEN
                          {
			    outputMode(); sollya_print_help_text("match");
                          }
                      | WITHTOKEN
                          {
			    outputMode(); sollya_print_help_text("with");
                          }
                      | ABSOLUTETOKEN
                          {
			    outputMode(); sollya_print_help_text("absolute");
                          }
                      | DECIMALTOKEN
                          {
			    outputMode(); sollya_print_help_text("decimal");
                          }
                      | RELATIVETOKEN
                          {
			    outputMode(); sollya_print_help_text("relative");
                          }
                      | FIXEDTOKEN
                          {
			    outputMode(); sollya_print_help_text("fixed");
                          }
                      | FLOATINGTOKEN
                          {
			    outputMode(); sollya_print_help_text("floating");
                          }
                      | ERRORTOKEN
                          {
			    outputMode(); sollya_print_help_text("error");
                          }
                      | QUITTOKEN
                          {
			    outputMode(); sollya_print_help_text("quit");
                          }
                      | FALSEQUITTOKEN
                          {
			    outputMode(); sollya_print_help_text("quit");
                          }
                      | RESTARTTOKEN
                          {
			    outputMode(); sollya_print_help_text("restart");
                          }
                      | LIBRARYTOKEN
                          {
			    outputMode(); sollya_print_help_text("library");
                          }
                      | LIBRARYCONSTANTTOKEN
                          {
			    outputMode(); sollya_print_help_text("libraryconstant");
                          }
                      | DIFFTOKEN
                          {
			    outputMode(); sollya_print_help_text("diff");
                          }
                      | BASHEVALUATETOKEN
                          {
			    outputMode(); sollya_print_help_text("bashevaluate");
                          }
                      | GETSUPPRESSEDMESSAGESTOKEN
                          {
			    outputMode(); sollya_print_help_text("getsuppressedmessages");
                          }
                      | GETBACKTRACETOKEN
                          {
			    outputMode(); sollya_print_help_text("getbacktrace");
                          }
                      | DIRTYSIMPLIFYTOKEN
                          {
			    outputMode(); sollya_print_help_text("dirtysimplify");
                          }
                      | REMEZTOKEN
                          {
			    outputMode(); sollya_print_help_text("remez");
                          }
                      | ANNOTATEFUNCTIONTOKEN
                          {
			    outputMode(); sollya_print_help_text("annotatefunction");
                          }
                      | MINTOKEN
                          {
			    outputMode(); sollya_print_help_text("min");
                          }
                      | MAXTOKEN
                          {
			    outputMode(); sollya_print_help_text("max");
                          }
                      | FPMINIMAXTOKEN
                          {
			    outputMode(); sollya_print_help_text("fpminimax");
                          }
                      | HORNERTOKEN
                          {
			    outputMode(); sollya_print_help_text("horner");
                          }
                      | EXPANDTOKEN
                          {
			    outputMode(); sollya_print_help_text("expand");
                          }
                      | SIMPLIFYSAFETOKEN
                          {
			    outputMode(); sollya_print_help_text("simplify");
                          }
                      | TAYLORTOKEN
                          {
			    outputMode(); sollya_print_help_text("taylor");
                          }
                      | TAYLORFORMTOKEN
                          {
			    outputMode(); sollya_print_help_text("taylorform");
                          }
                      | CHEBYSHEVFORMTOKEN
                          {
			    outputMode(); sollya_print_help_text("chebyshevform");
                          }
                      | AUTODIFFTOKEN
                          {
			    outputMode(); sollya_print_help_text("autodiff");
                          }
                      | DEGREETOKEN
                          {
			    outputMode(); sollya_print_help_text("degree");
                          }
                      | NUMERATORTOKEN
                          {
			    outputMode(); sollya_print_help_text("numerator");
                          }
                      | DENOMINATORTOKEN
                          {
			    outputMode(); sollya_print_help_text("denominator");
                          }
                      | SUBSTITUTETOKEN
                          {
			    outputMode(); sollya_print_help_text("substitute");
                          }
                      | COMPOSEPOLYNOMIALSTOKEN
                          {
			    outputMode(); sollya_print_help_text("composepolynomials");
                          }
                      | BEZOUTTOKEN
                          {
			    outputMode(); sollya_print_help_text("bezout");
                          }
                      | COEFFTOKEN
                          {
			    outputMode(); sollya_print_help_text("coeff");
                          }
                      | SUBPOLYTOKEN
                          {
			    outputMode(); sollya_print_help_text("subpoly");
                          }
                      | ROUNDCOEFFICIENTSTOKEN
                          {
			    outputMode(); sollya_print_help_text("roundcoefficients");
                          }
                      | RATIONALAPPROXTOKEN
                          {
			    outputMode(); sollya_print_help_text("rationalapprox");
                          }
                      | ACCURATEINFNORMTOKEN
                          {
			    outputMode(); sollya_print_help_text("accurateinfnorm");
                          }
                      | ROUNDTOFORMATTOKEN
                          {
			    outputMode(); sollya_print_help_text("round");
                          }
                      | EVALUATETOKEN
                          {
			    outputMode(); sollya_print_help_text("evaluate");
                          }
                      | LENGTHTOKEN
                          {
			    outputMode(); sollya_print_help_text("length");
                          }
                      | OBJECTNAMETOKEN
                          {
			    outputMode(); sollya_print_help_text("objectname");
                          }
                      | PARSETOKEN
                          {
			    outputMode(); sollya_print_help_text("parse");
                          }
                      | PRINTTOKEN
                          {
			    outputMode(); sollya_print_help_text("print");
                          }
                      | PRINTXMLTOKEN
                          {
			    outputMode(); sollya_print_help_text("printxml");
                          }
                      | READXMLTOKEN
                          {
			    outputMode(); sollya_print_help_text("readxml");
                          }
                      | PLOTTOKEN
                          {
			    outputMode(); sollya_print_help_text("plot");
                          }
                      | PRINTHEXATOKEN
                          {
			    outputMode(); sollya_print_help_text("printdouble");
                          }
                      | PRINTFLOATTOKEN
                          {
			    outputMode(); sollya_print_help_text("printsingle");
                          }
                      | PRINTBINARYTOKEN
                          {
			    outputMode(); sollya_print_help_text("printbinary");
                          }
                      | SUPPRESSMESSAGETOKEN
                          {
			    outputMode(); sollya_print_help_text("suppressmessage");
                          }
                      | UNSUPPRESSMESSAGETOKEN
                          {
			    outputMode(); sollya_print_help_text("unsuppressmessage");
                          }
                      | PRINTEXPANSIONTOKEN
                          {
			    outputMode(); sollya_print_help_text("printexpansion");
                          }
                      | BASHEXECUTETOKEN
                          {
			    outputMode(); sollya_print_help_text("bashexecute");
                          }
                      | EXTERNALPLOTTOKEN
                          {
			    outputMode(); sollya_print_help_text("externalplot");
                          }
                      | WRITETOKEN
                          {
			    outputMode(); sollya_print_help_text("write");
                          }
                      | ASCIIPLOTTOKEN
                          {
			    outputMode(); sollya_print_help_text("asciiplot");
                          }
                      | RENAMETOKEN
                          {
			    outputMode(); sollya_print_help_text("rename");
                          }
                      | BINDTOKEN
                          {
			    outputMode(); sollya_print_help_text("bind");
                          }
                      | INFNORMTOKEN
                          {
			    outputMode(); sollya_print_help_text("infnorm");
                          }
                      | SUPNORMTOKEN
                          {
			    outputMode(); sollya_print_help_text("supnorm");
                          }
                      | FINDZEROSTOKEN
                          {
			    outputMode(); sollya_print_help_text("findzeros");
                          }
                      | FPFINDZEROSTOKEN
                          {
			    outputMode(); sollya_print_help_text("fpfindzeros");
                          }
                      | DIRTYINFNORMTOKEN
                          {
			    outputMode(); sollya_print_help_text("dirtyinfnorm");
			  }
                      | GCDTOKEN
                          {
			    outputMode(); sollya_print_help_text("gcd");
			  }
                      | EUCLDIVTOKEN
                          {
			    outputMode(); sollya_print_help_text("div");
			  }
                      | EUCLMODTOKEN
                          {
			    outputMode(); sollya_print_help_text("mod");
			  }
                      | NUMBERROOTSTOKEN
                          {
			    outputMode(); sollya_print_help_text("numberroots");
                          }
                      | INTEGRALTOKEN
                          {
			    outputMode(); sollya_print_help_text("integral");
                          }
                      | DIRTYINTEGRALTOKEN
                          {
			    outputMode(); sollya_print_help_text("dirtyintegral");
                          }
                      | WORSTCASETOKEN
                          {
			    outputMode(); sollya_print_help_text("worstcase");
                          }
                      | IMPLEMENTPOLYTOKEN
                          {
			    outputMode(); sollya_print_help_text("implementpoly");
			  }
                      | INTERPOLATETOKEN
                          {
			    outputMode(); sollya_print_help_text("interpolate");
			  }
                      | IMPLEMENTCONSTTOKEN
                          {
			    outputMode(); sollya_print_help_text("implementconstant");
                          }
                      | CHECKINFNORMTOKEN
                          {
			    outputMode(); sollya_print_help_text("checkinfnorm");
                          }
                      | ZERODENOMINATORSTOKEN
                          {
			    outputMode(); sollya_print_help_text("zerodenominators");
                          }
                      | ISEVALUABLETOKEN
                          {
			    outputMode(); sollya_print_help_text("isevaluable");
                          }
                      | SEARCHGALTOKEN
                          {
			    outputMode(); sollya_print_help_text("searchgal");
                          }
                      | GUESSDEGREETOKEN
                          {
			    outputMode(); sollya_print_help_text("guessdegree");
                          }
                      | DIRTYFINDZEROSTOKEN
                          {
			    outputMode(); sollya_print_help_text("dirtyfindzeros");
                          }
                      | IFTOKEN
                          {
			    outputMode(); sollyaPrintf("If construct: if condition then command or if condition then command else command.\n");
                          }
                      | THENTOKEN
                          {
			    outputMode(); sollyaPrintf("If construct: if condition then command or if condition then command else command.\n");
                          }
                      | ELSETOKEN
                          {
			    outputMode(); sollyaPrintf("If construct: if condition then command else command\n");
                          }
                      | FORTOKEN
                          {
			    outputMode(); sollyaPrintf("For construct: for i from const to const2 [by const3] do command\nor for i in list do command.\n");
                          }
                      | INTOKEN
                          {
			    outputMode(); sollya_print_help_text("in");
                          }
                      | FROMTOKEN
                          {
			    outputMode(); sollyaPrintf("For construct: for i from const to const2 [by const3] do command.\n");
                          }
                      | TOTOKEN
                          {
			    outputMode(); sollyaPrintf("For construct: for i from const to const2 [by const3] do command.\n");
                          }
                      | BYTOKEN
                          {
			    outputMode(); sollyaPrintf("For construct: for i from const to const2 by const3 do command.\n");
                          }
                      | DOTOKEN
                          {
			    outputMode(); sollyaPrintf("For construct: for i from const to const2 [by const3] do command.\n");
			    outputMode(); sollyaPrintf("While construct: while condition do command.\n");
                          }
                      | beginsymbol
                          {
			    outputMode(); sollyaPrintf("Begin-end construct: begin command; command; ... end.\n");
                          }
                      | endsymbol
                          {
			    outputMode(); sollyaPrintf("Begin-end construct: begin command; command; ... end.\n");
                          }
                      | WHILETOKEN
                          {
			    outputMode(); sollyaPrintf("While construct: while condition do command.\n");
                          }
                      | INFTOKEN
                          {
			    outputMode(); sollya_print_help_text("inf");
                          }
                      | MIDTOKEN
                          {
			    outputMode(); sollya_print_help_text("mid");
                          }
                      | SUPTOKEN
                          {
			    outputMode(); sollya_print_help_text("sup");
                          }
                      | EXPONENTTOKEN
                          {
			    outputMode(); sollya_print_help_text("exponent");
                          }
                      | MANTISSATOKEN
                          {
			    outputMode(); sollya_print_help_text("mantissa");
                          }
                      | PRECISIONTOKEN
                          {
			    outputMode(); sollya_print_help_text("precision");
                          }
                      | EXECUTETOKEN
                          {
			    outputMode(); sollya_print_help_text("execute");
                          }
                      | ISBOUNDTOKEN
                          {
			    outputMode(); sollya_print_help_text("isbound");
                          }
                      | VERSIONTOKEN
                          {
			    outputMode(); sollyaPrintf("Prints the version of the software.\n");
                          }
                      | EXTERNALPROCTOKEN
		          {
			    outputMode(); sollya_print_help_text("externalproc");
			  }
                      | EXTERNALDATATOKEN
		          {
			    outputMode(); sollya_print_help_text("externaldata");
                          }
                      | VOIDTOKEN
		          {
			    outputMode(); sollya_print_help_text("void");
                          }
                      | CONSTANTTYPETOKEN
		          {
			    outputMode(); sollya_print_help_text("constant");
			  }
                      | FUNCTIONTOKEN
		          {
			    outputMode(); sollya_print_help_text("function");
			  }
                      | OBJECTTOKEN
		          {
			    outputMode(); sollya_print_help_text("object");
                          }
                      | RANGETOKEN
		          {
			    outputMode(); sollya_print_help_text("range");
                          }
                      | INTEGERTOKEN
		          {
			    outputMode(); sollya_print_help_text("integer");
                          }
                      | STRINGTYPETOKEN
		          {
			    outputMode(); sollya_print_help_text("string");
                          }
                      | BOOLEANTOKEN
		          {
			    outputMode(); sollya_print_help_text("boolean");
                          }
                      | LISTTOKEN
		          {
			    outputMode(); sollya_print_help_text("list");
			  }
                      | OFTOKEN
		          {
			    outputMode(); sollya_print_help_text("of");
			  }
                      | VARTOKEN
		          {
			    outputMode(); sollya_print_help_text("var");
                          }
                      | NOPTOKEN
		          {
			    outputMode(); sollya_print_help_text("nop");
                          }
                      | PROCTOKEN
		          {
			    outputMode(); sollya_print_help_text("proc");
                          }
                      | PROCEDURETOKEN
		          {
			    outputMode(); sollya_print_help_text("procedure");
                          }
                      | RETURNTOKEN
		          {
			    outputMode(); sollya_print_help_text("return");
                          }
                      | HELPTOKEN
                          {
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "The old WARN_IF_NO_HELP_TEXT option is no longer supported. Run something like 'grep \"sollya_print_help_text\" parser.y | sed -n -e 's/^.*sollya_print_help_text(\"\\([^\"]*\\)\");.*$/\\1/gp;' | sort | while read topic; do  if ! grep -q '\"'\"$topic\"'\"' sollya-help.c; then echo \"$topic\"; fi; done' for a similar result."
#endif

			    outputMode(); sollyaPrintf("Type \"help <keyword>;\" for help on the keyword <keyword>.\nFor example type \"help implementpoly;\" for help on the command \"implementpoly\".\n\n");
			    sollyaPrintf("Possible keywords in %s are:\n",PACKAGE_NAME);
			    sollyaPrintf("- !\n");
			    sollyaPrintf("- !=\n");
			    sollyaPrintf("- &&\n");
			    sollyaPrintf("- (\n");
			    sollyaPrintf("- )\n");
			    sollyaPrintf("- *\n");
			    sollyaPrintf("- +\n");
			    sollyaPrintf("- ,\n");
			    sollyaPrintf("- -\n");
			    sollyaPrintf("- .\n");
			    sollyaPrintf("- ...\n");
			    sollyaPrintf("- .:\n");
			    sollyaPrintf("- /\n");
			    sollyaPrintf("- :.\n");
			    sollyaPrintf("- :=\n");
			    sollyaPrintf("- ; (separator in ranges)\n");
			    sollyaPrintf("- <\n");
			    sollyaPrintf("- =\n");
			    sollyaPrintf("- ==\n");
			    sollyaPrintf("- >\n");
			    sollyaPrintf("- @\n");
			    sollyaPrintf("- {\n");
			    sollyaPrintf("- [|\n");
			    sollyaPrintf("- |]\n");
			    sollyaPrintf("- ||\n");
			    sollyaPrintf("- }\n");
			    sollyaPrintf("- ~\n");
			    sollyaPrintf("- [\n");
			    sollyaPrintf("- ]\n");
			    sollyaPrintf("- ^\n");
			    sollyaPrintf("- _x_\n");
			    sollyaPrintf("- D\n");
			    sollyaPrintf("- DD\n");
			    sollyaPrintf("- DE\n");
			    sollyaPrintf("- HP\n");
			    sollyaPrintf("- QD\n");
			    sollyaPrintf("- Pi\n");
			    sollyaPrintf("- RD\n");
			    sollyaPrintf("- RN\n");
			    sollyaPrintf("- RU\n");
			    sollyaPrintf("- RZ\n");
			    sollyaPrintf("- SG\n");
			    sollyaPrintf("- TD\n");
			    sollyaPrintf("- abs\n");
			    sollyaPrintf("- absolute\n");
			    sollyaPrintf("- accurateinfnorm\n");
			    sollyaPrintf("- acos\n");
			    sollyaPrintf("- acosh\n");
			    sollyaPrintf("- annotatefunction\n");
			    sollyaPrintf("- asciiplot\n");
			    sollyaPrintf("- asin\n");
			    sollyaPrintf("- asinh\n");
			    sollyaPrintf("- atan\n");
			    sollyaPrintf("- atanh\n");
			    sollyaPrintf("- autodiff\n");
			    sollyaPrintf("- autosimplify\n");
			    sollyaPrintf("- bashevaluate\n");
			    sollyaPrintf("- bashexecute\n");
			    sollyaPrintf("- begin\n");
			    sollyaPrintf("- bezout\n");
			    sollyaPrintf("- binary\n");
			    sollyaPrintf("- bind\n");
			    sollyaPrintf("- boolean\n");
			    sollyaPrintf("- by\n");
			    sollyaPrintf("- canonical\n");
			    sollyaPrintf("- ceil\n");
			    sollyaPrintf("- chebyshevform\n");
			    sollyaPrintf("- checkinfnorm\n");
			    sollyaPrintf("- coeff\n");
			    sollyaPrintf("- composepolynomials\n");
			    sollyaPrintf("- constant\n");
			    sollyaPrintf("- cos\n");
			    sollyaPrintf("- cosh\n");
			    sollyaPrintf("- decimal\n");
			    sollyaPrintf("- default\n");
			    sollyaPrintf("- degree\n");
			    sollyaPrintf("- denominator\n");
			    sollyaPrintf("- diam\n");
			    sollyaPrintf("- dieonerrormode\n");
			    sollyaPrintf("- diff\n");
			    sollyaPrintf("- dirtyfindzeros\n");
			    sollyaPrintf("- dirtyinfnorm\n");
			    sollyaPrintf("- dirtyintegral\n");
			    sollyaPrintf("- dirtysimplify\n");
			    sollyaPrintf("- display\n");
			    sollyaPrintf("- div\n");
			    sollyaPrintf("- do\n");
			    sollyaPrintf("- double\n");
			    sollyaPrintf("- doubledouble\n");
			    sollyaPrintf("- doubleextended\n");
			    sollyaPrintf("- dyadic\n");
			    sollyaPrintf("- else\n");
			    sollyaPrintf("- end\n");
			    sollyaPrintf("- erf\n");
			    sollyaPrintf("- erfc\n");
			    sollyaPrintf("- error\n");
			    sollyaPrintf("- evaluate\n");
			    sollyaPrintf("- execute\n");
			    sollyaPrintf("- exp\n");
			    sollyaPrintf("- expand\n");
			    sollyaPrintf("- expm1\n");
			    sollyaPrintf("- exponent\n");
			    sollyaPrintf("- externaldata\n");
			    sollyaPrintf("- externalplot\n");
			    sollyaPrintf("- externalproc\n");
			    sollyaPrintf("- false\n");
			    sollyaPrintf("- file\n");
			    sollyaPrintf("- findzeros\n");
			    sollyaPrintf("- fixed\n");
			    sollyaPrintf("- floating\n");
			    sollyaPrintf("- floor\n");
			    sollyaPrintf("- for\n");
			    sollyaPrintf("- fpfindzeros\n");
			    sollyaPrintf("- fpminimax\n");
			    sollyaPrintf("- from\n");
			    sollyaPrintf("- fullparentheses\n");
			    sollyaPrintf("- function\n");
			    sollyaPrintf("- gcd\n");
			    sollyaPrintf("- getsuppressedmessages\n");
			    sollyaPrintf("- getbacktrace\n");
			    sollyaPrintf("- guessdegree\n");
			    sollyaPrintf("- halfprecision\n");
			    sollyaPrintf("- head\n");
			    sollyaPrintf("- help\n");
			    sollyaPrintf("- hexadecimal\n");
			    sollyaPrintf("- honorcoeffprec\n");
			    sollyaPrintf("- hopitalrecursions\n");
			    sollyaPrintf("- horner\n");
			    sollyaPrintf("- if\n");
			    sollyaPrintf("- interpolate\n");
			    sollyaPrintf("- implementpoly\n");
			    sollyaPrintf("- implementconstant\n");
			    sollyaPrintf("- in\n");
			    sollyaPrintf("- inf\n");
			    sollyaPrintf("- infnorm\n");
			    sollyaPrintf("- integer\n");
			    sollyaPrintf("- integral\n");
			    sollyaPrintf("- isbound\n");
			    sollyaPrintf("- isevaluable\n");
			    sollyaPrintf("- length\n");
			    sollyaPrintf("- library\n");
			    sollyaPrintf("- libraryconstant\n");
			    sollyaPrintf("- list\n");
			    sollyaPrintf("- log\n");
			    sollyaPrintf("- log10\n");
			    sollyaPrintf("- log1p\n");
			    sollyaPrintf("- log2\n");
			    sollyaPrintf("- mantissa\n");
			    sollyaPrintf("- match\n");
			    sollyaPrintf("- max\n");
			    sollyaPrintf("- mid\n");
			    sollyaPrintf("- midpointmode\n");
			    sollyaPrintf("- min\n");
			    sollyaPrintf("- mod\n");
			    sollyaPrintf("- nearestint\n");
			    sollyaPrintf("- numberroots\n");
			    sollyaPrintf("- nop\n");
			    sollyaPrintf("- numerator\n");
			    sollyaPrintf("- object\n");
			    sollyaPrintf("- objectname\n");
			    sollyaPrintf("- of\n");
			    sollyaPrintf("- off\n");
			    sollyaPrintf("- on\n");
			    sollyaPrintf("- parse\n");
			    sollyaPrintf("- perturb\n");
			    sollyaPrintf("- pi\n");
			    sollyaPrintf("- plot\n");
			    sollyaPrintf("- points\n");
			    sollyaPrintf("- postscript\n");
			    sollyaPrintf("- postscriptfile\n");
			    sollyaPrintf("- powers\n");
			    sollyaPrintf("- prec\n");
			    sollyaPrintf("- precision\n");
			    sollyaPrintf("- print\n");
			    sollyaPrintf("- printbinary\n");
			    sollyaPrintf("- printdouble\n");
			    sollyaPrintf("- printexpansion\n");
			    sollyaPrintf("- printfloat\n");
			    sollyaPrintf("- printhexa\n");
			    sollyaPrintf("- printsingle\n");
			    sollyaPrintf("- printxml\n");
			    sollyaPrintf("- proc\n");
			    sollyaPrintf("- procedure\n");
			    sollyaPrintf("- quad\n");
			    sollyaPrintf("- quit\n");
			    sollyaPrintf("- range\n");
			    sollyaPrintf("- rationalapprox\n");
			    sollyaPrintf("- rationalmode\n");
			    sollyaPrintf("- readfile\n");
			    sollyaPrintf("- readxml\n");
			    sollyaPrintf("- relative\n");
			    sollyaPrintf("- remez\n");
			    sollyaPrintf("- rename\n");
			    sollyaPrintf("- restart\n");
			    sollyaPrintf("- return\n");
			    sollyaPrintf("- revert\n");
			    sollyaPrintf("- round\n");
			    sollyaPrintf("- roundcoefficients\n");
			    sollyaPrintf("- roundcorrectly\n");
			    sollyaPrintf("- roundingwarnings\n");
			    sollyaPrintf("- searchgal\n");
			    sollyaPrintf("- showmessagenumbers\n");
			    sollyaPrintf("- simplify\n");
			    sollyaPrintf("- sin\n");
			    sollyaPrintf("- single\n");
			    sollyaPrintf("- sinh\n");
			    sollyaPrintf("- sort\n");
			    sollyaPrintf("- sqrt\n");
			    sollyaPrintf("- string\n");
			    sollyaPrintf("- subpoly\n");
			    sollyaPrintf("- substitute\n");
			    sollyaPrintf("- sup\n");
			    sollyaPrintf("- supnorm\n");
			    sollyaPrintf("- suppressmessage\n");
			    sollyaPrintf("- tail\n");
			    sollyaPrintf("- tan\n");
			    sollyaPrintf("- tanh\n");
			    sollyaPrintf("- taylor\n");
			    sollyaPrintf("- taylorform\n");
			    sollyaPrintf("- taylorrecursions\n");
			    sollyaPrintf("- then\n");
			    sollyaPrintf("- time\n");
			    sollyaPrintf("- timing\n");
			    sollyaPrintf("- to\n");
			    sollyaPrintf("- tripledouble\n");
			    sollyaPrintf("- true\n");
			    sollyaPrintf("- unsuppressmessage\n");
			    sollyaPrintf("- var\n");
			    sollyaPrintf("- verbosity\n");
			    sollyaPrintf("- version\n");
			    sollyaPrintf("- void\n");
			    sollyaPrintf("- while\n");
			    sollyaPrintf("- with\n");
			    sollyaPrintf("- worstcase\n");
			    sollyaPrintf("- write\n");
			    sollyaPrintf("- zerodenominators\n");
			    sollyaPrintf("\n");
                          }
;


