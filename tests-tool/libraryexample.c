#include <mpfi.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sollya.h>
#include "lto-compat.h"

EXPORTED_FUNC_DEF(int, foo, mpfi_t rop, mpfi_t op, int n) {
  (void) n;
  (void) op;
  mpfi_interv_ui(rop, 0, 1);
  return 0;
}

/* Externalproc designed to test non-regression of bug #21283 */
EXPORTED_FUNC_DEF(int, takeyourtime) {
  if (fork() == 0) {
    sleep(5);
    printf("After\n");
    exit(0);
  }
  return 1;
}

/* Symbols and a function to test externaldata */

EXPORTED_SYMB_DEF(int magic = 17);

static int __printMagicDoit(sollya_obj_t obj) {
  void *data;
  int *magic;
  
  if (sollya_lib_decompose_externaldata(&data, NULL, obj)) {
    magic = (int *) data;
    printf("The magic is %d\n", *magic);
    *magic = *magic + 1;
  }

  return 1;
}

EXPORTED_FUNC_DEF(int, printMagic, void **args) {
  return __printMagicDoit((sollya_obj_t) (args[0]));
}
